<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta http-equiv="Page-Enter"
	content="revealTrans(Duration=10.0,Transition=1)">
<link rel="icon" href="img/icon/kh_blog_icon.png">

<link
	href="${pageContext.request.contextPath}/build/styles/framework-styles.css?v=1.4"
	rel="stylesheet">






<link
	href="${pageContext.request.contextPath}/build/styles/styles.css?v=1.4"
	rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/trix/0.9.2/trix.css">










<base href="/kriticalhealthblogs/">

</head>




<body id="{{loginBodyIncluded}}">
	<div id="loading-bar-container"></div>
	<!-- <div ng-show="shouldHeaderBeIncluded" ng-include="'header.html'"></div> -->
	<div ng-show="!shouldHeaderBeIncluded"  ng-include="'publicHeader.html'"></div>
 <div ng-show="shouldHeaderBeIncluded"  ng-include="'blogHeader.html'"></div> 



	<div ng-view></div>



	<script
		src="${pageContext.request.contextPath}/build/scripts/frameworks.js?v=1.9"></script>
		
		


<script src="//cdnjs.cloudflare.com/ajax/libs/trix/0.9.2/trix.js"></script>

<script src="${pageContext.request.contextPath}/js/angular-trix.min.js"></script>
		
				
		<script
		src="${pageContext.request.contextPath}/src/scripts/app-services/filter.js?v=1.3"></script>
		
		
			<script
		src="${pageContext.request.contextPath}/src/scripts/app.js?v=1.7"></script>
		
		<script
		src="${pageContext.request.contextPath}/src/scripts/app-services/flash.service.js?v=1.2"></script>
		
		
		
		<script
		src="${pageContext.request.contextPath}/src/scripts/app-services/config.js?v=1.2"></script>
		
		
		<script
		src="${pageContext.request.contextPath}/src/scripts/app-services/fileUpload.service.js?v=1.2"></script>



	



	<script
		src="${pageContext.request.contextPath}/src/scripts/app-services/valformaUtil.js?v=1.9"></script>

	<script
		src="${pageContext.request.contextPath}/src/scripts/app-services/common.service.js?v=1.2"></script>
	<script
		src="${pageContext.request.contextPath}/src/scripts/app-services/authentication.service.js?v=1.3"></script>




	<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/changePassword.controller.js?v=1.3"></script>


	<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/user.controller.js?v=1.3"></script>


	<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/profileSearch.controller.js?v=1.4"></script>
	<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/resetPassword.controller.js?v=1.3"></script>

	
	<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/confirmEmail.controller.js?v=1.3"></script>

	<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/coupon.controller.js?v=1.6"></script>
	<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/success.controller.js?v=1.1"></script>
	<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/failure.controller.js?v=1.1"></script>
	<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/viewApp.controller.js?v=1.0"></script>
	<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/forgotPassword.controller.js?v=1.4"></script>
		<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/login.controller.js?v=1.5"></script>
		
			
		
			<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/home.controller.js?v=1.9"></script>
		
		
		
				<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/publicHeader.controller.js?v=2.0"></script>
		<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/blogHeader.controller.js?v=2.1"></script>
				<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/blog.controller.js?v=2.0"></script>
		
		<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/popularBlog.controller.js?v=2.0"></script>
				<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/blogAuthor.controller.js?v=2.0"></script>
				<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/blogCreate.controller.js?v=2.1"></script>

<script
		src="${pageContext.request.contextPath}/src/scripts/app-controllers/blogInner.controller.js?v=2.0"></script>






	<script>
		angular.module('app').filter('unsafe', function($sce) {
			return $sce.trustAsHtml;
		});
		$(window).load(function() {
			var toggle = false;
			var user = "jQuery404";
			var searchBoxText = "Type here...";
			var fixIntv;
			var fixedBoxsize = $('#fixed').outerHeight() + 'px';
			var Parent = $("#fixed"); // cache parent div
			var Header = $(".fixedHeader"); // cache header div
			var Chatbox = $(".userinput"); // cache header div
			Parent.css('height', '30px');

			Header.click(function() {
				toggle = (!toggle) ? true : false;
				if (toggle) {
					Parent.animate({
						'height' : fixedBoxsize
					}, 300);
				} else {
					Parent.animate({
						'height' : '30px'
					}, 300);
				}

			});
		});
	</script>
	
	 




	<div spacer="20"></div>
	<div  ng-include="'footer.html'"></div>

</body>
</html>
