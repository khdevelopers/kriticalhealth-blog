﻿(function () {
    'use strict';

  var app=  angular
        .module('app', ['ngResource', 'ngMessages','ui.router','ngRoute','ngSanitize','appFilters','ngCookies','infinite-scroll','fupApp','ngMaterial','CKEditorExample','ngAnimate','ui.bootstrap','dateParser','angular-loading-bar','HIGHERTHAN','YEARDROPDOWN','satellizer','xeditable', 'toastr' ,'vsGoogleAutocomplete','ngMap','angularTrix']);
  
        app.config(config)
        .run(run);

    
    
        
  

    
    
    config.$inject = ['$routeProvider', '$locationProvider','$mdThemingProvider','cfpLoadingBarProvider','$authProvider'];
    
    
    
    function config($routeProvider, $locationProvider,$mdThemingProvider,cfpLoadingBarProvider,$authProvider) {
        cfpLoadingBarProvider.latencyThreshold = 100;
    	
   
        $routeProvider
            .when('/', {
                templateUrl: 'login.html',
               
                	
            })
            .when('/regsuccess', {
                templateUrl: 'regsuccess.html'
            })
             .when('/forgotPassword', {
                templateUrl: 'forgotPassword.html'
            })
             .when('/confirmEmail', {
                templateUrl: 'confirmEmail.html'
            })
             .when('/resetPassword', {
                templateUrl: 'resetPassword.html'
            })
              .when('/changePassword', {
                templateUrl: 'changePassword.html'
           })
           
             .when('/network', {
                templateUrl: 'network.html?v=1.1'  
                	
            })
            .when('/activity', {
                templateUrl: 'activity.html?v=1.0',
                isUserName:false	
                	
            })
               .when('/activity/:userId', {
                templateUrl: 'activity.html',
                isUserName:true	
            })
               .when('/blog', {
            	isBlog:true,
                templateUrl: 'blog.html?v=1.0'
                	
               })
               .when('/postedJobs', {
                templateUrl: 'postedJobs.html'  	
                	
                	
            })
            
         .when('/blog-author/:userId', {
        	 isBlog:true,
                templateUrl: 'blog-author.html'
                	
            })
            
            .when('/blog-author', {
            	isBlog:true,
            	templateUrl: 'blog-author.html'
            })
             
            
            .when('/blog-create', {
            	isBlog:true,
            	templateUrl: 'postBlog.html'
            })
            .when('/blog-edit/:blogId', {
            	isBlog:true,
            	templateUrl: 'postBlog.html'
            })
            
            .when('/blog-inner/:id', {
            	isBlog:true,
                templateUrl: 'blog-inner.html'
                	
            })
            .when('/login', {
                templateUrl: 'login.html?v=1.0'
             
            })
         
            .when('/eavAttribute/:id', {
                templateUrl: 'eavAttribute.html'

            }).when('/eavAttribute', {
                templateUrl: 'eavAttribute.html'
            
            }).when('/group', {
                templateUrl: 'group.html'
            
            
            
            }).when('/persona', {
                templateUrl: 'persona.html'
            }).when('/updateProfile', {
                templateUrl: 'updateProfile.html'
            })
            .when('/eavAttributeSearch', {
                templateUrl: 'eavAttributeSearch.html'
            })
             .when('/groupSearch', {
                templateUrl: 'groupSearch.html'
            })
            .when('/eavAttributeSetSearch', {
                templateUrl: 'eavAttributeSetSearch.html'
            })
            .when('/eavAttributeSet', {
                templateUrl: 'eavAttributeSet.html'
            })
            .when('/eavAttributeSet/:id', {
                templateUrl: 'eavAttributeSet.html'

            })
            .when('/eavAttributeGroupNew/:id', {
                templateUrl: 'eavAttributeGroup.html',
            })
            .when('/eavAttributeGroup/:id', {
                templateUrl: 'eavAttributeGroup.html'

            })
               .when('/eavEntityAttributeNew/:id', {
                templateUrl: 'eavEntityAttribute.html',
                
                
                
            })
            
            .when('/download-app/:id', {
                templateUrl: 'download-app.html'

            })
            .when('/eavEntityAttribute/:id', {
                templateUrl: 'eavEntityAttribute.html'

            })
            
            .when('/eavAttributeOptionNew/:id', {
                templateUrl: 'eavAttributeOption.html',
                
                
                
            })
            .when('/eavAttributeOption/:id', {
                templateUrl: 'eavAttributeOption.html'

            })
            
            .when('/jobs', {
                templateUrl: 'jobs.html'

            })
            .when('/jobs/:userId', {
                templateUrl: 'jobs.html'

            })

             .when('job-view', {
                templateUrl: 'job-view.html'

            })
            
            .when('/job-view/:id', {
                templateUrl: 'job-view.html'

            })
             
            .when('/postJob', {
                templateUrl: 'postJob.html'

            })
            
            .when('/company-careers', {                                         
                templateUrl: 'company-careers.html'

            })
            
            .when('/company', {
                templateUrl: 'company.html'

            })
            
            .when('/groups-profile', {
                templateUrl: 'groups-profile.html'

            })
             .when('/groups-profile/:groupId', {
                templateUrl: 'groups-profile.html'

            })
            
            
            
             .when('/groups-my', {
                templateUrl: 'groups-my.html'

            })
                                                                 
            .when('/groups-jobs', {
                templateUrl: 'groups-jobs.html'

            })
             .when('/groups-discover', {
                templateUrl: 'groups-discover.html'

            })
             .when('/groups', {
                templateUrl: '  groups.html'

            })
            
            .when('/groups-members/:groupId', {
                templateUrl: 'groups-members.html'

            })
            .when('/network-import', {
                templateUrl: 'network-import.html'

            })
             .when('/messaging', {
                templateUrl: 'messaging.html'

            })
            
            .when('/messaging/:messangerId', {
                templateUrl: 'messaging.html'

            })
             .when('/jobs-search', {
                templateUrl: 'messaging.html'

            })
            .when('/lookUpCodes', {
                templateUrl: ' lookUpCodes.html'

            })
             .when('/lookUpCodes/:id', {
                templateUrl: 'lookUpCodes.html'

            })
            .when('/profileSearch', {
                templateUrl: 'profileSearch.html?v=1.0'

            })
             .when('/appliedJobs', {
                templateUrl: 'appliedJobs.html'

            })
 
            .when('/settings', {
                templateUrl: 'settings.html'

            })
 
            .when('/lookUpType', {
                templateUrl: 'lookUpType.html'
            })
            
            .when('/profile', {
                
     
            		templateUrl: 
				         
							'CommonProfile.html?v=1.3',
				
                
              
            
                /*controller: 'CompanyProfileController'*/
            })
            
             .when('/IndividualProfile', {
                templateUrl: 'profile.html',
              
            })
            
               .when('/IndividualProfile/:userId', {
                templateUrl: 'profile.html',
               
            })
            
             .when('/NurseProfile/:userId', {
                templateUrl: 'profile.html',
               
            })
            
             .when('/NurseProfile', {
                templateUrl: 'profile-nurse.html',
              
            })
            
            
            
            
            .when('/publicProfile/:userName', {
                templateUrl: 'company-edit.html?v=123',
               
                	
            })
            
            .when('/lookUpType/:id', {
                templateUrl: 'lookUpType.html'

            })
            .when('/lookUpTypesSearch', {
                templateUrl: 'lookUpTypesSearch.html'
                	

            })
            
            .when('/lookUpCodesNew/:id', {
                templateUrl: ' lookUpCodes.html'

            })
             .when('/lookUpCodes/:id', {
                templateUrl: 'lookUpCodes.html'
                	
                	
             })
             .when('/userSearch', {
                templateUrl: 'userSearch.html'
            })
            .when('/userEdit/:id', {
                templateUrl: 'userEdit.html'
            })
              .when('/userRoleNew/:id', {
                templateUrl: 'userRole.html',
            })
            .when('/userRole/:id', {
                templateUrl: 'userRole.html'
            })
             .when('/roleSave', {
                templateUrl: 'roleSave.html'
            })
             .when('/roleSave/:id', {
                templateUrl: 'roleSave.html'
            })
            .when('/roleFunctionJoinNew/:id', {
                templateUrl: 'roleFunctionJoin.html',
            })
            .when('/roleFunctionJoin/:id', {
                templateUrl: 'roleFunctionJoin.html'

            })
            .when('/notifications', {
                templateUrl: 'notifications.html',
                isNotification:true

            })
            
             .when('/friendrequests', {
                templateUrl: 'notifications.html',
                	 isFriendRequest:true
            })
            
             .when('/singleActivity/:activityId', {
                templateUrl: 'singleActivity.html'
            })
             .when('/myAppointments', {
                templateUrl: 'myAppointments.html'
            })
            .when('/myAppointments/:appointmentId', {
                templateUrl: 'myAppointments.html'
            })
            
            .when('/publicProfile/:userId', {
                templateUrl: 'publicProfile.html'
            })
              .when('/invitation', {
                templateUrl: 'invitation.html'
            })
            
            .when('/pageNotFound', {
                templateUrl: 'pageNotFound.html'
            }).when('/p/:userId', {
                templateUrl: 
                'CommonProfile.html?v=1.0'	,
                isUserId:true
            })

            .when('/:userId', {
                templateUrl: 
                'CommonProfile.html?v=1.0',
                 isUserName:true,
                 profilePage:true
            })
            
            
             .when('/facebook', {
                templateUrl: 'facebook.html'
            })
            
            
            .otherwise({ redirectTo: '/login' });
        
        
        $locationProvider.html5Mode({
        	  enabled: true,
        	 
        	});
        
        $authProvider.facebook({
            clientId: facebook_client_id
        });
        
        $authProvider.google({
            clientId: google_client_id
          });
        
        
    }

    run.$inject = ['$rootScope', '$location', '$http','$cookieStore','editableOptions'];
   
    function run($rootScope, $location,  $http,$cookieStore,editableOptions) {
    	editableOptions.theme = 'bs3';
        // keep user logged in after page refresh
    	
  
   
    	$rootScope.globals = $cookieStore.get('globals') || {};
    	$rootScope.profileDetails = $cookieStore.get('profileDetails') || {};
    
        if ($rootScope.globals.currentUser) {
            //$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
        	
        	alert("locationChange");
        	
       
            var nonRestrictedPage=true;
            $.each(['/profile', '/changePassword', '/network',
                    '/activity', '/blog;postedJobs',
                    '/blog-author', '/blog-create',
                    '/blog-edit', '/blog-inner',
                    '/eavAttribute', '/group', '/persona',
                    '/updateProfile', '/eavAttributeSearch',
                    '/groupSearch', '/eavAttributeSetSearch',
                    '/eavAttributeSet',
                    '/eavAttributeGroupNew',
                    '/eavAttributeGroup',
                    '/eavEntityAttribute','/download-app',
                    '/eavAttributeOptionNew',
                    '/eavAttributeOption', '/jobs',
                    '/job-view', '/postJob',
                    '/company-careers', '/company',
                    '/groups-profile', '/groups-my',
                    '/groups-jobs', '/groups-discover',
                    '/groups', '/groups-members',
                    '/network-import', '/messaging',
                    '/jobs-search', '/lookUpCodes',
                    '/profileSearch', '/appliedJobs',
                    '/settings', '/lookUpType', '/profile',
                    '/IndividualProfile', '/NurseProfile',
                    '/publicProfile', '/lookUpTypesSearch',
                    '/lookUpCodesNew', '/userSearch',
                    '/userEdit', '/userRoleNew', '/userRole',
                    '/roleSave', '/roleFunctionJoinNew',
                    '/roleFunctionJoin', '/notifications',
                    '/singleActivity', '/myAppointments'
                     ], function(index, value){            /*   if($location.path().includes(value))*/
            	
            	if($location.path().indexOf(value) > -1)
            	   {
            	   nonRestrictedPage=false;
            	   }
            });
            $rootScope.globals = $cookieStore.get('globals') || {};
            $rootScope.profileDetails = $cookieStore.get('profileDetails') || {};
            var loggedIn = $rootScope.globals.currentUser;
            if(($location.path()=='/login' || $location.path()=='/')||($location.path()=='/forgotPassword')||($location.path()=='/resetPassword')||($location.path()=='/facebook') ||($location.path()=='/confirmEmail') || ($location.path().toString().indexOf('/publicProfile/') >-1)|| ($location.path()=='/regsuccess'|| ($location.path()=='/invitation') || !loggedIn  ))
      		{
        
           $rootScope.shouldHeaderBeIncluded=false;
           $rootScope.loginBodyIncluded='startpage';
      		}
      	else{
      		$rootScope.shouldHeaderBeIncluded=true;
      		$rootScope.loginBodyIncluded='kh';
      	 } 
            if(($location.path()=='/blog' || $location.path()=='/blog-author/')||($location.path()=='/blog-create')||($location.path()=='/blog-edit/')||($location.path()=='/blog-inner')  )
        	{
        	$rootScope.shouldHeaderBeIncluded=false;
        	$rootScope.shouldBlogsHeaderBeIncluded=true;
        
        	
        	}
    
        if (!nonRestrictedPage && !loggedIn) {
        	 $location.path('/login');
            }
        });
    }
    

  
})();


