(function () {
    'use strict';

    angular
        .module('app')
        .controller('BugDetailsController', BugDetailsController);

    BugDetailsController.$inject = ['$location', 'CommonService','FlashService','$scope','$rootScope','$routeParams'];
    function BugDetailsController($location, CommonService,FlashService,$scope,$rootScope,$routeParams) {
        var bugDetailsController = this;
        
        bugDetailsController.sendBug = sendBug; 
      
        
        (function initController() {
        	  bugDetailsController.dataLoading=false;

        })();


        function sendBug() {
        	bugDetailsController.dataLoading = true;
        	bugDetailsController.bugDetail.suggestedDate = new Date();
        	bugDetailsController.bugDetail.suggestionUrl=$(location).attr('href');
        	CommonService.postData(false,'sweb/bugDetailsRest/sendBug', bugDetailsController.bugDetail).then(function (response) {
			           	 
			                if (response.success) {
			                  
			                	document.getElementById("specError").innerHTML="Your message sent succesfully";
			                	bugDetailsController.bugDetail={};
			                	bugDetailsController.dataLoading = false;
			                	
			                } else {
			               	
			               	 FlashService.Error(response.message);
			               	bugDetailsController.dataLoading = false;
			               }
			           
			            
			            });

       };
       
      
        
    }})();



