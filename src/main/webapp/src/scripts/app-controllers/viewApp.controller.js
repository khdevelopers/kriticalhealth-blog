
(function () {
	'use strict';

	angular
	.module('app')
	.controller('ViewAppController' , ViewAppController )['tableSort' ];


	ViewAppController.$inject = ['$location','FlashService','$scope','$rootScope','$routeParams','CommonService','toastr'];
	function ViewAppController($location,FlashService,$scope,$rootScope,$routeParams,CommonService,toastr) {
		var viewAppController = this ;
		//controller variables
		$scope.purchases = [];
		$scope.couponDetail = false;
		var startRecordAc=0;
		$scope.loadMoreClicked = true;
		var startRecordUc =0;
		$scope.alerts ={};	  	  
	    $scope.alerts['edu']=[];
	    $scope.showLoadmore =true;
		(function initController() {


			CommonService.getData(true,'sweb/couponsRest/secured/getAll/',{}).then(function(response){

				if(response){
					$scope.purchases = response.object;

				}	


			});


		})();

		$scope.loadActiveCoupons = function(purchaseId, isLoadMore){

			if(isLoadMore === undefined || !isLoadMore ){

				startRecordAc = 0;
				$scope.loadUsedCoupons(purchaseId, isLoadMore);
			}
			CommonService.getData(true,'sweb/couponsRest/secured/getActiveCoupons/'+purchaseId+'/'+startRecordAc+'/'+10,{}).then(function(response){

				if(response){
					
					if(response.length<10){
					      $scope.showLoadmore = false;
					}

					$scope.purchases.some(function(part, index) {

						if(part.id == purchaseId){
							if(isLoadMore ){
								if(response != null){
									response.forEach(function(item, ind) {
										$scope.purchases[index].activeCouponList.push(item);
									});
									
								};
							}else{
								$scope.purchases[index].activeCouponList = response;

							}
							return true;
						};
						

					});


				};    



			}).finally(function () {
				startRecordAc+=10;

			});

		}



		$scope.couponsForShare = function(purchaseId, selectedActiveCoupon){
			$scope.purchases.some(function(part, index) {
				if(part.id == purchaseId){
					if( $scope.purchases[index].selectedCouponList == null){
						$scope.purchases[index].selectedCouponList = [];
					}
					var ind = $scope.purchases[index].selectedCouponList.indexOf(selectedActiveCoupon);
					if(ind == -1){
						$scope.purchases[index].selectedCouponList.push(selectedActiveCoupon);
					}else{
						$scope.purchases[index].selectedCouponList.splice(ind, 1);
					};
					return true;
				};
				
			});
		};





		$scope.loadUsedCoupons = function(purchaseId, isLoadMore){

			if(isLoadMore === undefined || !isLoadMore ){
				startRecordUc = 0;

			}
			CommonService.getData(true,'sweb/couponsRest/secured/getUsedCoupons/'+purchaseId+'/'+startRecordUc+'/'+10,{}).then(function(response){

				if(response){

					$scope.purchases.some(function(part, index) {

						if(part.id == purchaseId){
							if(isLoadMore ){
								if(response != null){
									response.forEach(function(item, ind) {
										$scope.purchases[index].usedCouponList.push(item);
									});
								};
							}else{
								$scope.purchases[index].usedCouponList = response;

							}
							return true;
						};


					});


				};    



			}).finally(function () {
				startRecordUc+=10;

			});


		}	


		$scope.sendEmail = function(purchaseId, email) {

			$scope.purchases.some(function(part, index) {
				if(part.id == purchaseId && $scope.purchases[index].selectedCouponList != null && $scope.purchases[index].selectedCouponList.length > 0){
					$scope.purchases[index].description = email;
					CommonService.postData(true, 'sweb/couponsRest/secured/sendMailToUser',
							$scope.purchases[index]).then(
									function(response) {

										if (response.success) {
											 toastr.success('Email Sent  ');

										}else{

											 toastr.error('Email Failed   ');
										}

									});
					return true;

				};
			});


		};


		$scope.showCouponDetail = function(){
			if($scope.couponDetail == false){
				$scope.couponDetail = true;
			}else{
				$scope.couponDetail = false;
			}
		};



		/*flash messages interval*/
		function flashsetinterval(array){
			var maxLifespan = 5000;
			// check once per second
			var interval =setInterval(function checkItems(){

				array.forEach(function(item){
					var d= convertUtcToLocalTime(Date.now());
					var d1= convertUtcToLocalTime(item.createdAt);
					var diff= Math.abs(d-d1);
					if(diff > maxLifespan){
						array.shift(); 

						$scope.$apply();   // remove first item


						clearInterval(interval);

					}
				});
			},1000);
		}




	}})();