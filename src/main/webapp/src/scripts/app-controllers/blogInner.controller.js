(function () {
    'use strict';

    angular
        .module('app')
        .controller('BlogInnerController', BlogInnerController);

    BlogInnerController.$inject = ['$location','FlashService','$scope','$rootScope','$routeParams','CommonService'];
    function BlogInnerController($location,FlashService,$scope,$rootScope,$routeParams,CommonService) {
        var blogInnerController = this;
        blogInnerController.getBlogById=getBlogById;
        blogInnerController.recentPost=getBlogById;
        blogInnerController.getBlogsComment =getBlogsComment;
        blogInnerController.getReplies =getReplies
        blogInnerController .likedByToggle =likedByToggle
        blogInnerController.commentLikedByToggle =commentLikedByToggle
        blogInnerController.blogComment={};
        blogInnerController.blogCommentReply={};
        blogInnerController.blogCommentLike={};
        blogInnerController.commentLikedBy=[]
        blogInnerController.commentLikeStartRecord=[];
        blogInnerController.blogLike={};
        
       $scope.showReply=false;
       $scope.isPreviousComments  = true;

       
        
        /*blogInnerController.userId = $rootScope.globals.currentUser.id;*/
    	/* single blog post */
    	blogInnerController.blogId=$routeParams.id;
        blogInnerController.comments=[];
        blogInnerController.replies=[];
        
    	
    	
    	  if($rootScope.globals.currentUser!=undefined ){
    		  blogInnerController.loggedInUserId=$rootScope.globals.currentUser.id;
    		  blogInnerController.loggedInUserName=$rootScope.globals.currentUser.username;
    		  
          }
    	  else{
    		  
    		  
    		  blogInnerController.loggedInUserId=null;
    		  blogInnerController.loggedInUserName=null;
    	  }
         
        
      
        (function initController() {
        	
        	getBlogById();
             recentPost();
             
           
       
            	
            
          
            
        })();
        
        

    	  
    	
        
        
        
        /*Get Blog Information By BlogId */
        function getBlogById(){
        	
        	CommonService.getData(false,'sweb/blogRest/getSingleBlog/'+blogInnerController.blogId,{}).then(function(response){
        	
        		
        		

            	
            	if (response==""){
            		 $location.path('/pageNotFound');
            	}else{
            		blogInnerController.singleBlog=response;
            	
            		blogInnerController.singleBlog.postedOn=convertUtcToLocalTime(blogInnerController.singleBlog.postedOn);
            		//blogInnerController.comments[blogInnerController.blogId]=blogInnerController.singleBlog.comments;
            		
            		getBlogsComment(blogInnerController.blogId,true);
            		
            		blogInnerController.likedBy=blogInnerController.singleBlog.blogLikeVos;
            		
            		blogInnerController.singleBlog.noOfLikes=blogInnerController.likedBy.length;
            		
            		
            		blogInnerController.likedBy.forEach(function(likedBy){
            			
            			if( likedBy.userName== blogInnerController.loggedInUserName){
            				blogInnerController.singleBlog.likedByYou=  true;
            				
            			}
            			
            			
            			
            			
		    			
		    			
		    			/* pictureView */
     					
     					if (likedBy.profilePictureId  != null) {
     						likedBy.picture = getPictureUrl(likedBy.profilePictureId,'profile', likedBy.pictureName);
     					} else
     						likedBy.picture = "img/defaultprofile.png";
     					/* pictureView */
		    			
		    		});
            	
            	
        		
				if (blogInnerController.singleBlog.profilePictureId != null) {
					blogInnerController.singleBlog.picture =getPictureUrlWithExtension(blogInnerController.singleBlog.profilePictureId,'profile',blogInnerController.singleBlog.pictureName,"thumnail_70_70");
				} else
					blogInnerController.singleBlog.picture = "img/defaultprofile.png";
				/* pictureView */}
            	
            	
        	});
        	
        }
        
        
        /*Get Recent Post  Information For Recent Post Card */
        function recentPost(){
        	
        	CommonService.getData(false,'sweb/blogRest/getAllBlogs/' + blogInnerController.userId+'/'+null+'/'+0+'/'+3,{}).then(function(response){
              	blogInnerController.recentBlogs=response;
              	blogInnerController.recentBlogs.forEach(function(recentBlog) {
              		
                      if (recentBlog.profilePictureId != null) {
                      	recentBlog.picture = getPictureUrl( recentBlog.profilePictureId,'profile',recentBlog.pictureName);
                      } else
                      	recentBlog.picture = "img/defaultprofile.png";
                   
          		});
          	});
        	
        	
        }
        


        
        
   
        
       
       
        
  


             /*like and unlike for blogs*/
        $scope.likeUnlike=function like(blogId) {
    	  blogInnerController.dataLoading = true;
	
	     blogInnerController.blogLike.blogId=blogId;
	     blogInnerController.blogLike.likedByYou=blogInnerController.singleBlog.likedByYou;
	   
		CommonService.postData(true,'sweb/blogLikeRest/secured/save',blogInnerController.blogLike)
	 	   .then(function(response) {

            if (response.success) {
              //  AuthenticationService.SetCredentials(vm.username, vm.password);
            	getBlogById();	
            } else {
           	blogInnerController.dataLoading = false;
           }
       
        
        
	 		   
	 	   });
	   
	   

		   
		   
		   
	   //}
	  		
    
}; /*like end*/









/* getting comments on Blogs*/
function getBlogsComment(blogId,shouldStartFromStart)
 {
 	if(shouldStartFromStart==true){
 		blogInnerController.commentStartRecord={};
 		
 		blogInnerController.comments[blogId]=[];
 		
 		
 	}
 	
 	
 	if(blogInnerController.commentStartRecord[blogId]==undefined){
 		blogInnerController.commentStartRecord[blogId]=0;
 	}
 	
	CommonService.getData(false,'sweb/blogCommentRest/getBLogComments/'+blogInnerController.blogId+'/'+  blogInnerController.loggedInUserId+'/'+ blogInnerController.commentStartRecord[blogId]+'/'+3,{}).then(function(response){
 	 

 		    
 		    	if ((response.length)<3){
 		    		$scope.isPreviousComments  = false;
 		    	}
 		    	else{
 		    		$scope.isPreviousComments  = true;
 		    		
 		    	}
    			
 		    	
 		    	response.forEach(function(comments){ 
 		    		comments.commentedOn=convertUtcToLocalTime(comments.commentedOn);
 		    		blogInnerController.comments[blogId].unshift(comments);
                 	    
   					/* pictureView */
 		    		if (comments.currentProfilePictureId  != null) {
 		    			comments.picture = getPictureUrl(comments.currentProfilePictureId,'profile',comments.pictureName);
  					} else
  						comments.picture = "img/defaultprofile.png";
  					/* pictureView */
   					
   					
   					
              	
               	});
 		    	
 		    	
 		    	
 			   blogInnerController.commentStartRecord[blogId]= blogInnerController.commentStartRecord[blogId]+3;
 		    	
 		    	
 		    

 
 		
 		    
 		    
 		    
     		/*blogInnerController.busy = false;*/
     		});
 	 
 	
 };
 
 
 
 
 /*for getting replies */
  function getReplies(commentId,shouldStartFromStart)
 {
 	 
 	 if(shouldStartFromStart==true){
 		blogInnerController.replyStartRecord={};
  		
  		blogInnerController.replies[commentId]=[];
  		
  		
  	}
 	 
 	 if(blogInnerController.replyStartRecord[commentId]==undefined){
  		blogInnerController.replyStartRecord[commentId]=0;
  	}
  	
   
      CommonService.getData(false,'sweb/blogCommentRest/getRepliesOnBlogComment/'+commentId+'/'+  blogInnerController.loggedInUserId+'/'+ blogInnerController.replyStartRecord[commentId]+'/'+3,{}).then(function(response){
      
      
     if(response.length<3){
    	 $scope.isPreviousReplies=false;
    	 
     }
     else{
    	 $scope.isPreviousReplies=true;
     }
	    	
	    	response.forEach(function(replies){ 
	    		blogInnerController.replies[commentId].unshift(replies);
	    		replies.commentedOn=convertUtcToLocalTime(replies.commentedOn);
	    		
          	    
				/* pictureView */
	    		if (replies.currentProfilePictureId  != null) {
	    			replies.picture = getPictureUrl(replies.currentProfilePictureId,'profile',replies.pictureName);
				} else
					replies.picture = "img/defaultprofile.png";
				/* pictureView */
	    	});
	    	
	    	
	    	 blogInnerController.replyStartRecord[commentId]=blogInnerController.replyStartRecord[commentId]+3;
           
	   
          
    	 
    	
				
				
				
	    	
        	
	    	});
     
	    	
 }
     	 
  
  /*comment on blog*/
  $scope.comment= function comment(blogId) {
     	blogInnerController.dataLoading = true;   	 
     	
     	
     	 
     	
     	
     	blogInnerController.blogComment.blogId=blogId;
     	blogInnerController.createdDate=new Date();
   	 
     	
 
    	 blogInnerController.blogComment.comment=blogInnerController.com;
	
 
    	 
    
     
   	CommonService.postData(true,'sweb/blogCommentRest/secured/save',blogInnerController.blogComment)
	   .then(function(response) {
  	 
       if (response.success) {
      	
    	   blogInnerController.com=''
      		 
     getBlogById();
     
    
      	 
       	 
       } else {
      	
      	 FlashService.Error(response.message);
      	blogInnerController.dataLoading = false;
      }
  
   
   });   
     
     
     	

     	
         
    };/*comments end*/
  
     
    
    /*reply to comment*/
    $scope.reply= function reply(blogId,parentId,index) {
     	blogInnerController.dataLoading = true;
    	 
     	var currentComment=null;
   	var totalComments=blogInnerController.comments[blogId] ;
    	
   	totalComments.forEach(function(entry) {
 	    
      	  if(entry.commentId ==parentId )
      		  {
      		currentComment=entry;
      		
               }
      	
      	
 	 });
    	
    	
    	
    	
    	
    	
    	
    	blogInnerController.blogCommentReply.comment=$scope.blogInnerController.rep[index];
     /*	blogInnerController.blogCommentReply.userId=blogInnerController.userId;*/
     	blogInnerController.blogCommentReply.blogId=blogId;
     	blogInnerController.blogCommentReply.parentId=parentId;
     	
     	
       	CommonService.postData(true,'sweb/blogCommentRest/secured/save',blogInnerController.blogCommentReply)
  	   .then(function(response) {
      	 
           if (response.success) {
             //  AuthenticationService.SetCredentials(vm.username, vm.password);
          	 
        	   $scope.blogInnerController.rep[index]='';
          
        	   
        	   currentComment.noOfReplies++;
          	getReplies(parentId,true);
          	$scope.showReply=false;
       
          
          	 
          	
         
           	
           	FlashService.Success(response.message);
           	
           } else {
          	
          	 FlashService.Error(response.message);
          	blogInnerController.dataLoading = false;
          }
      
       
       });   
    	
     	

 		
         
    };/*reply*/
    

    
    //activity liked by
  


/*activity liked by */
$scope.loadMoreForLikedBy = function(blogId,reloadFromStart,likes) {
	$("#con-likebox-blog").modal('toggle');
	
	
	
/*	if(reloadFromStart==true){
	
		blogInnerController.likedBy =[];
		 blogInnerController.likeStartRecord=0;
		 
		
	}
	
	 if (blogInnerController.likeStartRecord==undefined){
		 blogInnerController.likeStartRecord=0;
		 
	 }
		CommonService.getData(false,'sweb/blogLikeRest/likedBy/'+blogId+'/'+  blogInnerController.likeStartRecord+'/'+3,{}).then(function(response){
	 
	 
		    if((response.length)<3)	
		    	{
		    	
		    	$scope.isBlogLikedBy=false;
		    	
		    	
		    	}
		    else{
		    	
		    	$scope.isBlogLikedBy=true;
		    	
		    }
		  
		    
		    
		    
                  
			
			response.forEach(function(likedBy){
		    			
		    			blogInnerController.likedBy.push(likedBy);
		    			 pictureView 
     					
     					if (likedBy.profilePictureId  != null) {
     						likedBy.picture = getPictureUrl(likedBy.profilePictureId,'profile', likedBy.pictureName);
     					} else
     						likedBy.picture = "img/defaultprofile.png";
     					 pictureView 
		    			
		    		});
			
		
			    $("#con-likebox-blog").modal('toggle');
		    		
		    		
		    		
		    
    	
		    blogInnerController.likeStartRecord=blogInnerController.likeStartRecord+3;	
    		
    		});*/
	 



};

/* comment and reply  like  unlike*/
$scope.commentLikeUnlike= function commentLike(commentId,blogId,replyId) {
	   blogInnerController.dataLoading = true;
	   
	 
	  var currentComment=null;
	  var currentReply=null;
	   
	   blogInnerController.comments[blogId].forEach(function(entry) {
        	    
       	  if(entry.commentId ==commentId )
       		  {
       		  
       		  
       		  currentComment=entry;
       		 
       		/*entry.noOfLikes++;
       		entry.likedByYou=true;*/
       		  }
       	
        	});
	   
	   if(replyId){
		   
			blogInnerController.replies[commentId].forEach(function(replyEntry) {
          	    
             	  if(replyEntry.commentId ==replyId )
             		  {
             		 currentReply=replyEntry;
             	/*	replyEntry.noOfLikes++;
             		replyEntry.likedByYou=true;*/
             		  }
             	
              	});
		   
	   }
	   
    	
		   
	   
	  if (replyId==undefined){
		  blogInnerController.blogCommentLike.commentId=commentId;
		  blogInnerController.blogCommentLike.likedByYou= currentComment.likedByYou;
		   
		CommonService.postData(true,'sweb/blogCommentLikeRest/secured/save', blogInnerController.blogCommentLike)
	 	   .then(function(response) {
           if (response.success) {
               //  AuthenticationService.SetCredentials(vm.username, vm.password);
             	 
                		 
                		
        	   if(response.object.likedByYou){
        		   currentComment.noOfLikes++;
           		currentComment.likedByYou=true;
        		   
        	   }
        	   else{
        		   
        		   currentComment.noOfLikes--;
              		currentComment.likedByYou=false;
        		   
        	   }
        	   
                		  
             	
             	 
             	
             	FlashService.Success(response.message);
             } else {
             	
            	
            	 FlashService.Error(response.message);
            	blogInnerController.dataLoading = false;
            }
        
         
         });
		  

	  }
	  else{
		  blogInnerController.blogCommentLike.commentId=replyId;
		  blogInnerController.blogCommentLike.likedByYou= currentReply.likedByYou;
		  
		CommonService.postData(true,'sweb/blogCommentLikeRest/secured/save',blogInnerController.blogCommentLike)
 	   .then(function(response) {
             if (response.success) {
                 //  AuthenticationService.SetCredentials(vm.username, vm.password);
 
               	
               	 
           		
          	   if(response.object.likedByYou){
          		 currentReply.noOfLikes++;
                	currentReply.likedByYou=true;
          		   
          	   }
          	   else{
          		   
          		 currentReply.noOfLikes--;
                	currentReply.likedByYou=false;
          		   
          	   }
               	 
               	 
               		
               	
                       		  
                       	
                       
               	
               	 
               	
               	FlashService.Success(response.message);
               } else {
               	
              	
              	 FlashService.Error(response.message);
              	blogInnerController.dataLoading = false;
              }
          
           
           });
	  

		  
	  }
	   
		   
		   
		   
	   //}
	  		
    
}; /*like end*/




//loadmore for comment like
$scope.commentLikedBy= function commentLikedBy(commentId,reloadFromStart,likes)
{
   
   if(reloadFromStart==true){
	   
	   blogInnerController.commentLikedBy=[];
	   blogInnerController.commentLikeStartRecord[commentId]=0;
	   
	   
   }
   
   
   

	
	 if (blogInnerController.commentLikeStartRecord[commentId]==undefined){
		 blogInnerController.commentLikeStartRecord[commentId]=0;
		 
	 }
	 
	 
		CommonService.getData(false,'sweb/blogCommentLikeRest/likedBy/'+commentId+'/'+  blogInnerController.commentLikeStartRecord[commentId]+'/'+3,{}).then(function(response){

			
		    if((response.length)<3)	
	    	{
	    	
	    	$scope.isCommentLikedBy=false;
	    	
	    	
	    	}
		    else{
		    	$scope.isCommentLikedBy=true;
		    	
		    }
			
		   
		    	blogInnerController.commentLikedBy=response;
		    	blogInnerController.commentLikes=likes;
		    	blogInnerController.commentId=commentId;
		    	
		 
		    	
		    	
		    	blogInnerController.commentLikedBy.forEach(function(commentLikedBy) {
		    		
		    		
		    		
             	    
					/* pictureView */
					
					if (commentLikedBy.profilePictureId != null) {
						commentLikedBy.picture =getPictureUrl(commentLikedBy.profilePictureId,'profile', commentLikedBy.pictureName);
					} else
						commentLikedBy.picture = "img/defaultprofile.png";
					/* pictureView */
          	
           	});
		    	
		    	
		    	
		    	
		   
  	
		  blogInnerController.commentLikeStartRecord[commentId]=blogInnerController.commentLikeStartRecord[commentId]+3;
		  
		  $("#con-likebox").modal('toggle');
  		/*blogInnerController.busy = false;*/
		
	 
	
  		});
   
   
};

/* delete blog Comment*/
$scope.deleteBlogComment= function deleteBlogComment(commentId,blogId,index){
	  
	CommonService.postData(false,'blogCommentRest/deleteBlogComment/'+commentId+'/'+  blogId,{}).then(function(response){

		blogInnerController.comments[blogId].splice(index,1);
	});
	
	   
	   
}

/* delete  reply*/
$scope.deleteCommentReply= function deleteCommentReply(commentId,parentId,index){
	  
	CommonService.postData(false,'blogCommentRest/deleteCommentReply/'+commentId+'/'+  parentId,{}).then(function(response){
		
		blogInnerController.replies[parentId].splice(index,1);
	});
	   
	   
}

/*$scope.scrollToCommentDiv=function scrollToCommentDiv(){
	
	$('#commentInput').focus();
	
	    $('html, body').animate({
	        scrollTop: $("#commentInput").offset().top
	    }, 2000);
	
	
}*/

/*$scope.scrollTo = function() {
	document.getElementById('commentInput').scrollTop -= 10;
	$('html,body').animate({
        scrollTop: $(".comment").offset().top},
        'slow');
  };*/

/*$('#commentscroll').live("click", function(e) {
	   e.preventDefault(); 

	   $('#commentInput').focus()

	   return false;
	})
*/


/*modal backdrop*/
function likedByToggle(blogId,userId,userName)
{
$location.path('/'+userName);
 $('.modal-backdrop').hide();
 $("body").css("overflow","auto");
// $('#myModal(blogId)').collapse('toggle');myModal
}

/*modal backdrop*/
function commentLikedByToggle(userName)
{
	$location.path('/'+userName);
	 $('.modal-backdrop').hide();
	 $("body").css("overflow","auto");
}

    

    
    
     	 
     	 
     	


  }})();



