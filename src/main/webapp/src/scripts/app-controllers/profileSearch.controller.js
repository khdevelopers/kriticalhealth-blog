(function () {
    'use strict';

    angular
        .module('app')
        .controller('ProfileSearchController', ProfileSearchController, ['ui.bootstrap','appFilters']);

    ProfileSearchController.$inject = ['$location', '$scope', 'FlashService','$routeParams','$rootScope','CommonService'];
    function ProfileSearchController($location,$scope, FlashService,$routeParams,$rootScope,CommonService) {
           var profileSearchController = this;
           
          
          
           profileSearchController. sendRequest=sendRequest;
           profileSearchController.modalTogal=modalTogal;
           profileSearchController.loadMore=loadMore;
           profileSearchController.profiles=[];
           $scope.isBusy = false;
           
           if($rootScope.globals.currentUser!=undefined){
        	   profileSearchController.userId=  $rootScope.globals.currentUser.id;
        	   
           }
           else{
        	   profileSearchController.userId=-1;
        	   
        	   
           }
         
          var  startRecord=0;
        	
        	
        	(function initController() {
        		
        	
        		loadMore();
        	
        })();
        	
        	
        	
        	/*get all users in kritical health with infinite scroll*/
        	function loadMore(){
        		
        		var searchObject = $location.search();
        		if(searchObject.q==undefined)
        			{
        			searchObject.q='';
        			}
        		if(searchObject.type==undefined)
    			{
    			searchObject.type='';
    			}
        		 
        		profileSearchController.loadingProfiles=true;	
           	 if($scope.isBusy === true) {
           		 return;
           	 }
           	 else
           		 {
           		 
           		
        		$scope.isBusy = true;
        		
        		CommonService.getData(false,'sweb/profileRest/getProfileVos/'+profileSearchController.userId+'/'+startRecord+'/'+10+'?q='+searchObject.q+'&type='+searchObject.type,{}).then(function(response){
        			
        		
    				
        			
        			response.forEach(function(profile){
        				profileSearchController.profiles.push(profile);
        				profileSearchController.profiles.forEach(function(profile) {
        					
        				
        					
                      	    
        					
        				
        					if (profile.currentPictureId != null) {
        						
        						profile.picture = getPictureUrlWithExtension(profile.currentPictureId,profile.type,profile.pictureName,"thumnail_300_300") ;
        					} else
        						profile.picture = "img/defaultprofile.png";
        					
                       	
                        	});
        				
        				
        				
        				
        			});
        			
        			  /* startRecord=startRecord+10;*/
        		
    				
                
              	   
              	}).finally(function () {
              		startRecord+=10;
                    $scope.infiniteScoll = false;
                    $scope.isBusy = false;
                   
                    profileSearchController.loadingProfiles=false;
                    
                
                });
        	}
        	}
        	
        	
        	
        	
        
        	 /*for modal backdrop on location change*/
        	 function modalTogal()
             {
           	 $('.modal-backdrop').hide();
           	 $("body").css("overflow","auto");
             }
        	 
        	 
        		/* sent request  */
     		function sendRequest(specializationId,userId) {
     			
     			if (specializationId==undefined){
     				specializationId=0;
     			}
     			if (specializationId==undefined){
    				specializationId=0;
    			}

    			CommonService.postData(true,'sweb/friendRequestRest/secured/sendRequest/'+userId+'/'+specializationId,{})
    		 	   .then(function(response) {

						
							if (response.success) {
								
								response = {
										success : true
									};
								
								profileSearchController.profiles.forEach(function (singleProfile){
			                  		  if(singleProfile.userId==userId)
		                  			  {
			                  			singleProfile.requestSent=true;
		                  			  }
		                    	});
								
							} else {
								FlashService.Error(response.message);
								companyProfileController.dataLoading = false;
							}

						});
               
     			 
                
     			
     				

     			
     		};/* sent request end */
        	 
        
        

         
        
       
    }
      
           

})();






