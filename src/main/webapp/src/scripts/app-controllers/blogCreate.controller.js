(function () {
    'use strict';

    angular
        .module('app')
        .controller('BlogCreateController', BlogCreateController);

    BlogCreateController.$inject = ['$location','FlashService','$scope','$rootScope','$routeParams','CommonService','fileUpload'];
    function BlogCreateController($location,FlashService,$scope,$rootScope,$routeParams,CommonService,fileUpload) {
        var blogCreateController = this;
        
        blogCreateController.update = update; 
        
        blogCreateController.userId=$rootScope.globals.currentUser.id;
        
        
        
     
        //blogCreateController.userId=$rootScope.globals.currentUser.id;
    	$scope.createblogAuthor="true";
        (function initController() {
        	
        	
        	blogCreateController.blogAuthor={};
        	blogCreateController.dataLoading =false;
        	$('.note-editable').html('');
          
        })();

       
       
        // getting values for edit method
        blogCreateController.blogAuthorId = $routeParams.blogId;
      
        if($routeParams.blogId){
        	 CommonService.getData(false,'sweb/blogRest/getSingleBlog/'+$routeParams.blogId,{}).then(function(response){

	        		blogCreateController.blogAuthor=response;
	        		blogCreateController.blogAuthor.postedOn=convertUtcToLocalTime(blogCreateController.blogAuthor.postedOn);
	        		$('.note-editable').html(blogCreateController.blogAuthor.content);
					/* pictureView */
	        	});
        	
        }
       
        
        /*Save or update depending on blogid*/
        function update() {
        	blogCreateController.dataLoading = true;
        	blogCreateController.blogAuthor.postedOn = new Date();
        	
         	// update 
            if(blogCreateController.blogAuthor.id!=null)
            {
            	 blogCreateController.blogAuthor.postedBy = blogCreateController.userId;
		       	  // blogCreateController.blogAuthor.content = $('#summercontent').summernote('code');
		       	$scope.isValid=$scope.bytesToSize(blogCreateController.blogAuthor.content);
		       		
		       	
		       	if(	$scope.isValid){
		       		CommonService.updateData(true,'sweb/blogRest/secured/update',blogCreateController.blogAuthor).then(function(response){
		       		
			           	 
		                if (response.success) {
		                  
		                	 response = { success: true ,message :"Saved succesfully"};
		                	
		                	
		                	FlashService.Success(response.message);
		                
		                	blogCreateController.blogAuthor={};
		                	$('.note-editable').html('');
		                	$location.path('/blog-author');
		                	blogCreateController.dataLoading=false;
		                	
		                	
		                	
		                } else {
		               	
		               
		               }
		           
		            
		            });
		       		
		       	}
		       	else{
		       	 FlashService.Error("Please Reduce The Size Of Blog");
		       	blogCreateController.dataLoading=false;
		       	 
		      	
		       	}
		      
            }
            else
        	{
			       	   blogCreateController.blogAuthor.postedBy = blogCreateController.userId;
			       	  /* blogCreateController.blogAuthor.content = $('#summercontent').summernote('code');*/
			       	$scope.isValid=$scope.bytesToSize(blogCreateController.blogAuthor.content);
			       		
			       	if($scope.isValid){
			       		CommonService.postData(true,'sweb/blogRest/secured/save',blogCreateController.blogAuthor).then(function(response){
			       
				           	 
			                if (response.success) {
			                
			                	 response = { success: true ,message :"Saved succesfully"};
			                     FlashService.Success(response.message);
			                     blogCreateController.blogAuthor={};
			                	 $('.note-editable').html('');
			                	 $location.path('/blog-author');
			                	 blogCreateController.dataLoading=false;
			                } else {
			               	
			               	 FlashService.Error(response.message);
			               	blogCreateController.dataLoading = false;
			               }
			           
			            
			            });
			       		
			       	}
			       	else{
			       		FlashService.Error("Please Reduce The Size Of Blog");
			        	blogCreateController.dataLoading = false;
			       		
			       	}
			       	
			       	
       		
        }

       };
       
       
       
       
       
       
       
       /* Upload a picture  without reference with type blog */
       /* saves returned picture id into a variable until you hit update  */
   	/* after hitting update it will update the picture table  refrence id as saved row primary key */
       /* Fires on blog picture  change event*/
		 $scope.blogOnChange = function readURL1(input) {
			 
			 $scope.uploadingBlogPicture=true;
			 $scope.uploadingblog=true;
			 if (input.files && input.files[0]) {
				 if(window.FileReader)
					 {
					 var reader = new FileReader();
			         reader.onload = (function(theFile) {
						
						return function(e) {
							//obj.src = e.target.result;
							var img = new Image();
							img.onload = function () {
								 var canvas = document.createElement('canvas');
							        var ctx = canvas.getContext('2d');
							        canvas.width = 600;
							       
							      
							        canvas.height = canvas.width * (img.height / img.width);
							        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

							        // SEND THIS DATA TO WHEREVER YOU NEED IT
							        var data = canvas.toDataURL('image/png');
							    	var uploadUrl = projectUrl + "sweb/pictureRest/base64FileUploadWithoutRef/blog";
									
							    	
									
									fileUpload.uploadBase64FileToUrl(data, input.files[0].name, uploadUrl, function (response){
										
											/* pictureView */
											if (response!= -1) {
												
												blogCreateController.blogPictureId=response;
												$scope.uploadingBlogPicture=false;
												$scope.uploadingblog=false;
												$scope.showRemoveSign=true;
												 blogCreateController.blogAuthor.imageUrl=getPictureUrl(response,'blog',input.files[0].name);  
												 
												
												  return "uploadSuccesFully";
											} else
												{
												return "uploadFail";
												
												}
											/* pictureView */
									});

							};
							img.src =  e.target.result;
							$('#blogPic').attr('src', e.target.result);
						
						
							//uploadTeam(theFile);
						};
					})
			        
			      (input.files[0]);
					
			        reader.readAsDataURL(input.files[0]);
			    
					 }
				 else
					 {


						var data=input.files[0];
						var uploadUrl = projectUrl + "sweb/pictureRest/myUploadWithoutRef/blog";
						$scope.uploadingblog=true;
			        	fileUpload.uploadFileToUrl(data, uploadUrl, function (response){
	        				
    					
    					/* pictureView */
    					if (response!= -1) {
    						blogCreateController.blogPictureId=response;
    						$scope.uploadingBlogPicture=false;
							$scope.uploadingblog=false;
							
    						
						
							   blogCreateController.blogAuthor.imageUrl=getPictureUrl(response,'blog',data.name);
    						
    						  
    						$('#blogPic').attr('src',  getPictureUrl(response,'blog',data.name));
    						
    					} else
    						{
    					/*	picturepath = "img/kh_icons/group-logo.png";*/
    						}
    			
    					
    	
    	           
    	        
    			});
						
					 }
			        }
	};
	
       
       
       /*blog size should not be more than 5 mb validation method*/
     $scope.bytesToSize=   function bytesToSize(bytes) {
    	 if(bytes.length< 5*(1024*1024))
    		 {
    		 return true;
    		 }
    	 else{
    		 return false;
    	 }
    	
    	};
    	
    	$scope.imageUpload = function(input) {
    	    console.log('image upload:', input);
    	    console.log('image upload\'s editable:', $scope.editable);
    	    
    	   
    	    

			 
			 $scope.uploadingBlogPicture=true;
			 $scope.uploadingblog=true;
			 if (input && input[0]) {
				 if(window.FileReader)
					 {
					 var reader = new FileReader();
			         reader.onload = (function(theFile) {
						
						return function(e) {
							//obj.src = e.target.result;
							var img = new Image();
							img.onload = function () {
								 var canvas = document.createElement('canvas');
							        var ctx = canvas.getContext('2d');
							        canvas.width = 600;
							       
							      
							        canvas.height = canvas.width * (img.height / img.width);
							        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

							        // SEND THIS DATA TO WHEREVER YOU NEED IT
							        var data = canvas.toDataURL('image/png');
							    	var uploadUrl = projectUrl + "sweb/pictureRest/base64FileUploadWithoutRef/blog";
									
							    	
									
									fileUpload.uploadBase64FileToUrl(data, input[0].name, uploadUrl, function (response){
										
											/* pictureView */
											if (response!= -1) {
												
												blogCreateController.blogPictureId=response;
												$scope.uploadingBlogPicture=false;
												$scope.uploadingblog=false;
												$scope.showRemoveSign=true;
												 //blogCreateController.blogAuthor.imageUrl=getPictureUrl(response,'blog',input[0].name);  
												 

												 console.log(getPictureUrl(response,'blog',input[0].name));
												 $scope.editor.summernote('editor.insertImage',  getPictureUrl(response,'blog',input[0].name));
												 
												
												
												
												  return "uploadSuccesFully";
											} else
												{
												return "uploadFail";
												
												}
											/* pictureView */
									});

							};
							img.src =  e.target.result;
							
							$scope.editable
							$('#blogPic').attr('src', e.target.result);
						
						
							//uploadTeam(theFile);
						};
					})
			        
			      (input[0]);
					
			        reader.readAsDataURL(input[0]);
			    
					 }
				 else
					 {


						var data=input[0];
						var uploadUrl = projectUrl + "sweb/pictureRest/myUploadWithoutRef/blog";
						$scope.uploadingblog=true;
			        	fileUpload.uploadFileToUrl(data, uploadUrl, function (response){
	        				
   					
   					/* pictureView */
   					if (response!= -1) {
   						blogCreateController.blogPictureId=response;
   						$scope.uploadingBlogPicture=false;
							$scope.uploadingblog=false;
							
   						
						
							   blogCreateController.blogAuthor.imageUrl=getPictureUrl(response,'blog',data.name);
   						
   						  
   						$('#blogPic').attr('src',  getPictureUrl(response,'blog',data.name));
   						
   					} else
   						{
   					/*	picturepath = "img/kh_icons/group-logo.png";*/
   						}
   			
   					
   	
   	           
   	        
   			});
						
					 }
			        }
	
    	    
    	    
    	  }
       
      
        
    }})();



