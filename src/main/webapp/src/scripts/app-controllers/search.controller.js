(function () {
    'use strict';

    angular
        .module('app')
        .controller('SearchController', SearchController);

    SearchController.$inject = ['$location','FlashService','$scope','$rootScope','$routeParams','$cookieStore','$route'];
    function SearchController($location,FlashService,$scope,$rootScope,$routeParams,$cookieStore,$route) {
    	var searchController = this;
    	if($route.current.isBlog){
    		searchController.placeholder="Search In Blogs";
   	 }
   	 else{
   		searchController.placeholder="Search Profiles";
   	 }
        
        $scope.search=function search()
      {
    	 if($route.current.isBlog){
    		  $location.url('/blog?q='+(searchController.q==undefined?'':searchController.q));
    		}
    	  else{
    		  $location.url('/profileSearch?q='+(searchController.q==undefined?'':searchController.q));
    		//  document.getElementById("myNumber").placeholder = "Amount"
    		  
    	  }
    	  
    	 
      }
        
        $scope.searchDoctor=function searchDoctor()
      {
    	 
    		  $location.url('/profileSearch?type='+0);
    		//  document.getElementById("myNumber").placeholder = "Amount"
    		  
    	  }
    	  
    	 
        $scope.searchCompany=function searchCompany()
        {
      	 
      		  $location.url('/profileSearch?type='+1);
      		
      		  
      	  }
        
        $scope.searchGroup=function searchGroup()
        {
        
      		  $location.url('/groups-discover?q=');
      		
      		  
      	  }

       
        
    }})();


