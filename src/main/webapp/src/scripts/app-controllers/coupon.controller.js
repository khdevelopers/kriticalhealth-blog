(function() {
	'use strict';

	angular.module('app').controller('MakeCouponController',
			MakeCouponController);

	MakeCouponController.$inject = [ '$location', 'FlashService', '$scope',
	                                 '$rootScope', '$routeParams', 'CommonService', 'toastr' ];
	function MakeCouponController($location, FlashService, $scope, $rootScope,
			$routeParams, CommonService, toastr) {
		var makeCouponController = this;
		//controller variables
		$scope.selectedAppPlatform = {};
		$scope.itemList = true;
		$scope.itemDetail = false;
		$scope.itemCart = false;
		$scope.androidPlatform = false;
		$scope.applePlatform = false;
		$scope.isLoading = false;
		$rootScope.couponResponseObject = {};
		$scope.selectedAppName = {};
		$scope.alerts = {};
		$scope.AppName = {};
		$scope.itemsForPurchase = [];
		$scope.allAppDetails = {};

		makeCouponController.getExistingCart = getExistingCart;

		(function initController() {

			CommonService.getData(true,
					'sweb/couponsRest/secured/getAllAppsForPurchase/', {})
					.then(function(response) {

						$scope.AppName = response;

					});
			getExistingCart();

		})();

		function getExistingCart() {
			CommonService
			.getData(true, 'sweb/couponsRest/secured/getExistingCart',
					{})
					.then(
							function(response) {

								$scope.allAppDetails = response.object;
								$scope.itemsForPurchase = $scope.allAppDetails.appPurchases;
								localStorage.setItem("itemsInCart",
										$scope.allAppDetails);

								$scope.itemDetail = false;
								if($scope.itemsForPurchase.length > 0){ 
									$scope.itemCart = false;
									$scope.itemList = true;
								}else{
									$scope.itemCart = false;
									$scope.itemList = true;
								}


							});

		}

		$scope.showCartView = function() {

			$scope.itemList = false;
			$scope.itemDetail = false;
			$scope.itemCart = true;
		};

		$scope.showItemCart = function() {

			var isPresent = false;
			var isQuantityChanged = false;

			if ($scope.selectedAppName.quantity == 0) {

				toastr.error('Please enter quantity ');
				return;

			}

			$scope.itemsForPurchase.forEach(function(item, index) {
				if (item.appId == $scope.selectedAppKey) {

					if ($scope.selectedAppName.quantity == item.quantity) {
						isPresent = true;
						isQuantityChanged = false;
						toastr.error('This APP is already in the cart ');

					} else {
						isPresent = true
						isQuantityChanged = true;
						var index = index;

					}

				}

			});

			if (isPresent && !isQuantityChanged) {
				$scope.showItemList();
				return;
			} else if (isPresent && isQuantityChanged) {

				if (index) {
					$scope.itemsForPurchase.splice(index, 1);
					$scope.itemsForPurchase.push($scope.selectedAppName);

				}

			} else {
				$scope.itemsForPurchase.push($scope.selectedAppName);
				

			}

			var isPresent = false;

			if (isPresent) {
				$scope.showItemList();
				return;
			}

			CommonService
			.postData(true, 'sweb/couponsRest/secured/calculate',
					$scope.itemsForPurchase)
					.then(
							function(response) {

								if (response.success) {

									$scope.allAppDetails = response.object;
									$scope.itemsForPurchase = $scope.allAppDetails.appPurchases;
									localStorage.setItem("itemsInCart",
											$scope.allAppDetails);

									$scope.itemList = false;
									$scope.itemDetail = false;
									$scope.itemCart = true;

								}
							});

		};
		
	

		$scope.deleteFromItemsForPurchase = function deleteFromItemsForPurchase(
				index) {
			
			
			 bootbox.confirm("Are you sure you want to delete this app from the cart?", function(result) {
                 if(result == true){
                	 
             		$scope.allAppDetails.removeAppId = $scope.allAppDetails.appPurchases[index].appId;

        			CommonService.postData(true,
        					'sweb/couponsRest/secured/removeFromCheckout',
        					$scope.allAppDetails).then(function(response) {

        						if (response.success) {
        				    $scope.allAppDetails = response.object;
        					$scope.AppName[$scope.allAppDetails.removeAppId].quantity=0;

        							localStorage.setItem("itemsInCart", $scope.allAppDetails);

        						}
        					});

        			$scope.itemsForPurchase.splice(index, 1);
        			//put succes message deleted success fully

        			$scope.totalPrice = 0;

        			$scope.itemsForPurchase.forEach(function(item) {

        				$scope.totalPrice = $scope.totalPrice + item.amount;

        			});

        			$scope.totalPrice.toFixed(2);
                	 
                		}
             });
			

	

		};

		$scope.openModal = function openModal(){
      	  
      	  
      	
                 	
                  	
      		  		

      		  		$("#termsModel").modal("toggle");
      			  
      		
          		
      		 
      		  
              
              };
      	  
		
		
		
		
		$scope.showItemList = function() {
			$scope.itemList = true;
			$scope.itemDetail = false;
			$scope.itemCart = false;
		};

		$scope.showItemDetail = function(key) {

			$scope.selectedAppName = $scope.AppName[key];
			$scope.selectedAppKey = key;

			console.log($scope.selectedAppName);
			
			if($scope.selectedAppName.quantity==0){
				$scope.selectedAppName.quantity=1;
			}
			
			$scope.calcLogicForUserDisplay($scope.selectedAppName);

			$scope.selectedAppPlatform = $scope.selectedAppName.platform;
			$scope.itemList = false;
			$scope.itemDetail = true;
			$scope.itemCart = false;

			if ($scope.selectedAppPlatform == "BOTH") {

				$scope.androidPlatform = true;
				$scope.applePlatform = true;
			}
			if ($scope.selectedAppPlatform == "android") {

				$scope.androidPlatform = true;
				$scope.applePlatform = false;
			}
			if ($scope.selectedAppPlatform == "apple") {

				$scope.androidPlatform = false;
				$scope.applePlatform = true;
			}
			
			
			
		};

		// dropdown name of app and cost

		$scope.checkOut = function() {

			$scope.itemsForPurchase.forEach(function(item) {

				item.id = null;

			});
			CommonService.postData(true,
					'sweb/couponsRest/secured/savePurchase',
					$scope.allAppDetails).then(
							function(response) {

								if (response.success) {
									var savedCoupons = response.object;
									//url save

									localStorage.setItem("url",
											savedCoupons.redirectURL);
									localStorage.setItem("totalAmount",
											savedCoupons.totalAmount);
									localStorage.setItem("orderId",
											savedCoupons.orderId);
									localStorage.setItem("requestId",
											savedCoupons.requestId);

									window.location.href = savedCoupons.redirectURL;
								}
							});

		};
		
		
		
		$scope.calcLogicForUserDisplay=function calcLogicForUserDisplay(selectedAppName){
			
			
			if(selectedAppName.discperc!=0 && selectedAppName.discqt!=0 ){
				$scope.newPrice=(selectedAppName.discqty*(selectedAppName.price-(selectedAppName.discperc*(selectedAppName.price/100))))+(selectedAppName.quantity-selectedAppName.discqty)*selectedAppName.price;
				$scope.youSavedPrice=(selectedAppName.price*selectedAppName.quantity)-$scope.newPrice;
				
			}
			else if(selectedAppName.promoprice!=null  ){
				$scope.newPrice=selectedAppName.promoprice*selectedAppName.quantity;
				$scope.youSavedPrice=(selectedAppName.price*selectedAppName.quantity)-$scope.newPrice;
				
				
			}
			else{
				
			}
			
		
			
			
			
			
		};
		

	}
})();