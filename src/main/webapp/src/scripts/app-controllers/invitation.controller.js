(function () {
    'use strict';

    angular
        .module('app')
        .controller('InvitationController', InvitationController);

    InvitationController.$inject = ['$location','FlashService','$scope','$rootScope','$routeParams','CommonService'];
    function InvitationController($location,FlashService,$scope,$rootScope,$routeParams,CommonService) {
        var invitationController = this;
        
        invitationController.sendInvitation = sendInvitation;  
        
        invitationController.invitation={};
        
        
      
        
        (function initController() {

        	
        	
        	
        })();

        
        
        
        function sendInvitation(invitationType) {
        	invitationController.dataLoading = true;
        
        	
        	
        		if(invitationType=="getLink")
           		{	
        			
        			CommonService.postData(true,'sweb/invitationRest/getInvitationLink',invitationController.invitation).then(function(response){
            
    		       			invitationController.invitationLink=response.object;
    		                if (response.success) {
    		                
    		                	invitationController.invitation={};
    		                	$scope.message=true;
    		
    		                	response = { success: true };
    		                	 FlashService.Success(response.message);
    		                
    		                
    		                } else {
    		               	
    		               	 FlashService.Error(response.message);
    		               	invitationController.dataLoading = false;
    		               }
    		           
    		            
    		            });
           		}
            	else if(invitationType=="defaultMail")
           		{	
    		       		
            		CommonService.postData(true,'sweb/invitationRest/sendInvitationDefaultMail',invitationController.invitation).then(function(response){
            		
    		           	 
    		                if (response.success) {
    		
    		                	response = { success: true ,message :"Invitation send successfully"};
    		                	
    		                	
    		                	invitationController.invitation={};
    		               	 FlashService.Success(response.message);
    		                
    		                
    		                } else {
    		               	
    		                	response = { success: false ,message :"Some Error Accured Try Get Link Button To Generate Link !!"};
    		               	 FlashService.Error(response.message);
    		               	invitationController.dataLoading = false;
    		               }
    		           
    		            
    		            });
           		}
               	else if(invitationType=="getText")
           		{	
               		$("#invitation-text-blk").show();
               		CommonService.postData(true,'sweb/invitationRest/sendInvitation',invitationController.invitation).then(function(response){
    		       	
    		       			invitationController.invitation=response.object;
    		           	 
    		                if (response.success) {
    		                 
    		                } else {
    		               	
    		               	 FlashService.Error(response.message);
    		               	invitationController.dataLoading = false;
    		               }
    		           
    		            
    		            });
           		}
            	
               	else if(invitationType=="textMail")
           		{	
               		
               		CommonService.postData(true,'sweb/invitationRest/sendInvitationWithText',invitationController.invitation).then(function(response){
    	       
    	       			invitationController.invitation=response.object;
    	           	 
    	                if (response.success) {
    	                      
    	                	response = { success: true ,message :"Invitation send successfully"};
    	                	$("#invitation-text-blk").hide();
    	                	
    	                
    	                } else {
    	               	
    	               	 FlashService.Error(response.message);
    	               	invitationController.dataLoading = false;
    	               }
    	           
    	            
    	            })
    	       		
           		}
        		           		
       };
        
    }})();


