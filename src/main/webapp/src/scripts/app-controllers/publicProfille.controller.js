(function() {
	'use strict';

	angular.module('app').controller('PublicProfileController', PublicProfileController);

	PublicProfileController.$inject = [ '$location', 'ProfileService', 'UserService','ProfileExpService','PrivacySettingsService','QualificationService','MessagesService','AwardsService',
			'EavAttributeSetService', 'ActivityStreamService', 'FlashService',
			'$scope', '$rootScope', '$routeParams', 'FollowService', '$http',
			'FriendService', 'fileUpload','$cookieStore'];
	function PublicProfileController($location, ProfileService, UserService,ProfileExpService,PrivacySettingsService,QualificationService,MessagesService,AwardsService,
			EavAttributeSetService, ActivityStreamService, FlashService,
			$scope, $rootScope, $routeParams, FollowService, $http,
			FriendService, fileUpload, $cookieStore)
	{
		var publicProfileController = this;

		//publicProfileController.acceptRequest = acceptRequest;
		//publicProfileController.update = update;
		//publicProfileController.follow = follow;
		//publicProfileController.sendRequest = sendRequest;
		//publicProfileController.rejectRequest = rejectRequest;
		
		//publicProfileController.updateExp=updateExp;
		//publicProfileController.updateQualification=updateQualification;
		
		//publicProfileController.isFollowing = false;

		//publicProfileController.saveMessage = saveMessage;
		publicProfileController.profile = {};
		
		/*if($rootScope.globals.currentUser.id== undefined){
			$rootScope.globals.currentUser.id=$routeParams.userId;
			
		}*/
			publicProfileController.userId = $routeParams.userId;

		(function initController() {
			
			
			getGalleryPictures();
			
			
			ProfileExpService.getProfileExpByUserId(publicProfileController.userId, function (response) {
				publicProfileController.profileExps=response;
				if(response)
					{
			     publicProfileController.profileExps.forEach(function(profileExpSingle) {
			              	    
								/* pictureView */
								
								if (profileExpSingle.currentProfilePictureId != null) {
									profileExpSingle.picture = projectUrl
											+ 'sweb/pictureRest/viewPicture/'
											+ profileExpSingle.currentProfilePictureId;
								} 
								else
									profileExpSingle.picture = "img/job-deficon.png";
								/* pictureView */
			               	
			                	});
					}
			});
			
			QualificationService.getQualificationByUserId(publicProfileController.userId, function (response) {
				publicProfileController.allQualifications=response;
			    
			});
			
/* getting all Awards from awards table for User */
			
			AwardsService.getAwardsByUserId(publicProfileController.userId, function (response) {
				publicProfileController.allAwards=response;
				if(response)
				{
						publicProfileController.allAwards.forEach(function(singleAward) {
		              	    
							/* pictureView */
							
							if (singleAward.currentAwardPictureId != null) {
								singleAward.picture = projectUrl
										+ 'sweb/pictureRest/viewPicture/'
										+ singleAward.currentAwardPictureId;
							} 
							/* pictureView */
		               	
		                	});
				}
			    
			});
			
			
			
		
				ProfileService.getProfileUpdateVo(publicProfileController.userId, function(response) {
							publicProfileController.profile = response;
							var mydate = new Date(publicProfileController.profile.dob);
							publicProfileController.profile.dob=mydate.toLocaleString();
							if(publicProfileController.profile.dob=="1/1/1970, 5:30:00 AM")
								{
									publicProfileController.profile.dob="";
								}
							else
								{
									publicProfileController.profile.dob=mydate;
								}
							
							publicProfileController.profile.country=publicProfileController.searchCountryText;
							
							$('.note-editable').html(publicProfileController.profile.aboutUs);
							/* pictureView */
							if (publicProfileController.profile.currentProfilePictureId != null) {
								publicProfileController.profile.profilePicture = projectUrl
										+ 'sweb/pictureRest/viewPicture/'
										+ publicProfileController.profile.currentProfilePictureId;
							} else
								publicProfileController.profile.profilePicture = "img/defaultprofile.png";
							/* pictureView */

						});
			
			
			
			/*
			 * pictureView
			 * 
			 * ProfileService.viewPicture(publicProfileController.profile.currentProfilePictureId,function(response) {
			 * publicProfileController.ProfilePicture=response; });
			 * 
			 * pictureView
			 */

			/* To Get EAV Attribiute SET Values */

		/*	EavAttributeSetService.search('{}', -1, 0, function(response) {
				publicProfileController.eavAttributeSets = response;
			});

			
			     EavAttributeSetService.getCompleteTree(publicProfileController.profile.eavAttributeId,function(response) {
						publicProfileController.profile.eavAttributeSet = response;
					});*/
			
			
			

			// Start getting no of posts by user id
			ActivityStreamService.getNoOfPostsByUserId(
					publicProfileController.userId, function(response) {
						publicProfileController.userPosts = response;
					});

			// end

			
			// reset login status
			// UserService.ClearCredentials();
			UserService.getUserDetailsByUserId(publicProfileController.userId, function(
					response) {
				publicProfileController.userDetails = response;
			});
			
			// code for getting privacy settings 
			
			publicProfileController.privacySettingSearch={};
			publicProfileController.privacySettingSearch.userId=$routeParams.userId;
			if($routeParams.userId!=null)
        	{
			
			PrivacySettingsService.getAllSettings(publicProfileController.privacySettingSearch, -1, 0,function (response) {
        		publicProfileController.allSettings=response;
        		
        	
        		publicProfileController.allSettings.forEach(function(singleSetting) {
	              	    
						/* pictureView */
        			if (singleSetting.settingType=="EMAIL"){
        			
        			    $scope.showEmail=PrivacySettingsService.getSingleSetting(singleSetting,publicProfileController.isFriend);
        			}
        			else if (singleSetting.settingType=="MOBILE"){
            			
        			    $scope.showMobile=PrivacySettingsService.getSingleSetting(singleSetting,publicProfileController.isFriend);
        			}
        			else if (singleSetting.settingType=="DOB"){
            			
        			    $scope.showDob=PrivacySettingsService.getSingleSetting(singleSetting,publicProfileController.isFriend);
        			}
						
						
	               });
        		

        	});
    			
        	}
        	else
        		{
        		$scope.showEmail=true;
    			$scope.showMobile=true;
    			$scope.showDob=true;
        		}
        	
		})();
		
		function getGalleryPictures()
		{
			ProfileService.getGalleryPictureIds(publicProfileController.userId,-1,0,function (response){
				publicProfileController.galleryPictureIds=response;
				
				
				publicProfileController.galleryPictureIds.pictures=[];
				
				
				
				publicProfileController.galleryPictureIds.forEach(function(galleryPicture) {
		       	    
						/* pictureView */
						
						if (galleryPicture.galleryPictureId != null) {
							galleryPicture.pictures = projectUrl
									+ 'sweb/pictureRest/viewPicture/'
									+ galleryPicture.galleryPictureId;
						} 
						/* pictureView */
		        	
		         	});
				
			});
		}
		
		
	}
	
	
	
	
})();
