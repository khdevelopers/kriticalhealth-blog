(function () {
    'use strict';

    angular
        .module('app')
        .controller('BlogAuthorController', BlogAuthorController);

    BlogAuthorController.$inject = ['$location','FlashService','$scope','$rootScope','$routeParams','CommonService'];
    function BlogAuthorController($location,FlashService,$scope,$rootScope,$routeParams,CommonService) {
        var blogAuthorController = this;
        
        
        
        
        blogAuthorController.deleteBlogAuthor=deleteBlogAuthor;
        
        blogAuthorController.getBlogsByUserId=getBlogsByUserId;
        
        blogAuthorController.getRecentPosts=getRecentPosts;
        blogAuthorController.logout=logout;
        
        
        if ($routeParams.userId != null && $routeParams.userId!=$rootScope.globals.currentUser.id){
        	
        	blogAuthorController.userId=$routeParams.userId;
			$scope.createblog="false";
			}
			else
				{
				blogAuthorController.userId=$rootScope.globals.currentUser.id;
					$scope.createblog="true";
				}
        
        
      
    	$scope.createblogAuthor="true";
        (function initController() {
        	
        	blogAuthorController.blogAuthor={};
        	$('.note-editable').html('');
           
        	
      
        		getBlogsByUserId();
        		
        	
        
        	
        	getRecentPosts();
            
            
        })();
        
        
        
        /*get union of my blogs and the person you are following blogs*/
        function getBlogsByUserId()
        
       {
        	
        	 CommonService.postData(false,'sweb/blogRest/getBlogsByUserId/' + blogAuthorController.userId+'/'+-1+'/'+0,{}).then(function(response){
        	
        		blogAuthorController.blogAuthors=response;
        		
        		
        		blogAuthorController.blogAuthors.forEach(function(blogAuthor) {
        			
        			blogAuthor.content=removeImageTag(blogAuthor.content);
        			
        			
        			
                    if (blogAuthor.profilePictureId != null) {
                    	blogAuthor.picture = projectUrl
                       + 'sweb/pictureRest/viewPicture/'
                       + blogAuthor.profilePictureId;
                    } else{
                    	blogAuthor.picture = "img/defaultprofile.png";
                    }
                 
        		});
        	});
        	
       }
        
        
        
  	  function removeImageTag(htmlString){
  	  
  		  
  		var content = htmlString.replace(/<img[^>]*>/g,"");
  	
  		  
  		
  		  
  		  return content;
  		  
  		  
  	  
    }
        
        
        
        
          //get More recent 3 blogs
        function getRecentPosts()
        {
        	 CommonService.getData(false,'sweb/blogRest/getAllBlogs/'+null+'/'+0+'/'+3,{}).then(function(response){
       
        		blogAuthorController.recentBlogAuthors=response;
        		blogAuthorController.recentBlogAuthors.forEach(function(recentBlogAuthor) {
                    if (recentBlogAuthor.profilePictureId != null) {
                    	recentBlogAuthor.picture = getPictureUrl( recentBlogAuthor.profilePictureId,'profile',recentBlogAuthor.pictureName);
                    } else
                    	recentBlogAuthor.picture = "img/defaultprofile.png";
                 
        		});
        	});
        }
        
        
       
        
      
       // delete my blogs
       
       function deleteBlogAuthor(blogAuthorId,index){
    	   CommonService.postData(true,'sweb/blogRest/secured/deleteBlog/'+blogAuthorId,{}).then(function(response){
    		   
    	
    	   
    		   blogAuthorController.blogAuthors.splice(index,1);
  	   });
    	   
       }
       
       
       
       
       /*logout function */
       /*clear coockies*/
       function logout()
     {
       	document.cookie = 'access_token' + "=" + "; expires="
			+ 'Thu, 01 Jan 1970 00:00:00 GMT'
			+ "; domain=.kriticalhealth.com; path=/";
       	localStorage.setItem("access_token", '');
       	document.cookie = 'globals' +"=" + JSON.stringify($rootScope.globals) + "; expires=" + 'Thu, 01 Jan 1970 00:00:00 GMT' 
           + "; domain=.kriticalhealth.com; path=/";
   	 $("#profile-top-menu").toggle();
   	 location.reload();
   	 $location.path('/');
     }
   
       
       
        
    }})();



