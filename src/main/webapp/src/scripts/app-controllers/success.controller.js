(function() {
	'use strict';

	angular.module('app').controller('SuccessController', SuccessController);

	SuccessController.$inject = [ '$location', 'FlashService', '$scope',
			'$routeParams', 'CommonService', 'toastr' ];
	function SuccessController($location, FlashService, $scope, $routeParams,
			CommonService, toastr) {
		var successController = this;
		$scope.couponResponseObject = {};
		$scope.alerts ={};	  	  
	    $scope.alerts['edu']=[];
		$scope.notAllowed = false;
		//controller variables

		successController.activateCoupon = activateCoupon;
		successController.setDataAfterSuccessOrFailure = setDataAfterSuccessOrFailure;

		(function initController() {

			activateCoupon();

		})();

		function setDataAfterSuccessOrFailure(response) {
			$scope.couponResponseObject.refresh = false;
			if(response.success){
			$scope.couponResponseObject.status = 1;
			}else{
				$scope.couponResponseObject.status = 2;
			}
			if ($scope.couponResponseObject != undefined) {
				$scope.buttonStatus = false;

				if ($scope.couponResponseObject.status == 1) {
					$scope.couponResponseObject.apppurchases = response.object;
					toastr.success('Payment successful ');

				} else {
					//don't display amount and paymentId in case of failure
					//in case of failure response.object is an apppurchase object
					if(response.object.description && response.object.description =='REFRESH'){
						$scope.couponResponseObject.refresh = true;	
						toastr.error('Already Coupon generated !!');
					}else{					
					
						toastr.error('Payment unsuccessful');
					}

				}

			} else {

				$scope.buttonStatus = true;
			}

		}

		$scope.printFunction = function() {
			window.print();
		}
		$scope.cancelCoupon = function() {

			var failureMessageObject = {

				orderId : localStorage.getItem("orderId")

			};

			CommonService.postData(true, 'sweb/couponsRest/secured/cancel',
					failureMessageObject).then(function(response) {

				if (response.success) {

					localStorage.removeItem("totalAmount");
					localStorage.removeItem("url");
					$location.path("/download-app");

				}

			});

		}

		$scope.repayCoupon = function() {

			//url save

			CommonService.postData(
					true,
					'sweb/couponsRest/secured/createPayment',
					'{"requestId":"' + $scope.couponResponseObject.requestId
							+ '"}').then(function(response) {

				if (response.success) {
					window.location.href = localStorage.getItem("url");

				} else {
					//	alert("error");
				}
			});

		}

		$scope.sendEmail = function() {

			CommonService.postData(true, 'sweb/couponsRest/secured/sendMail',
					$scope.couponResponseObject.apppurchases).then(
					function(response) {

						if (response.success) {

							$scope.alerts['edu'] = [ {
								type : 'success',
								createdAt : Date.now(),
								msg : "Email sent..!!"
							} ];

						}

					});

		}

		function activateCoupon() {

			if ($location.search().payment_id
					&& $location.search().payment_request_id) {

				var appPurchase = {
					paymentId : $location.search().payment_id,
					orderId : localStorage.getItem("orderId")

				};

				CommonService.postData(true,
						'sweb/couponsRest/secured/activate', appPurchase).then(
						function(response) {

							if (response.success) {
								setDataAfterSuccessOrFailure(response);
							} else {

								setDataAfterSuccessOrFailure(response);

							}
						});
			} else {

				$scope.notAllowed = true;

			}

		}

	}
})();