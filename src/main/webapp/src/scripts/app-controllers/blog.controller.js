(function () {
    'use strict';

    angular
        .module('app')
        .controller('BlogController', BlogController);

    BlogController.$inject = ['$location','FlashService','$scope','$rootScope','$routeParams','$route','CommonService'];
    function BlogController($location,FlashService,$scope,$rootScope,$routeParams,$route,CommonService) {
        var blogController = this;
        
        
        
        
        if($rootScope.globals.currentUser!=undefined){
        	
        	   blogController.userId=$rootScope.globals.currentUser.id;
        	
        }

     
        $scope.isBusy=false;
        blogController.search=search;
        blogController.blogs=[];
        blogController.loadMore=loadMore;
        
        var startRecord=1;
       
        (function initController() {
        	
        	loadMore();
        	
        	
        	
        	
        	
        })();
        
        if($route.current.isBlog){
        	
        }
        
        
    	  function removeImageTag(htmlString){
    	  
    		  
    		var content = htmlString.replace(/<img[^>]*>/g,"");
    	
    		  
    		
    		  
    		  return content;
    		  
    		  
    	  
      }
         
    
        function search(){
        	

  		  $location.url('/blog?q='+(blogController.q==undefined?'':blogController.q));
        	
        	
        }
        
        /*get blogs  for search with infinite scroll*/
    	function loadMore(){
    		var searchObject = $location.search();
    		if (!searchObject.q){
    			searchObject.q=null;
    			
    		}
    		
    		blogController.loadingBlogs=true;
    		if($scope.isBusy=== true)
    			{
    			return;
    			}
    		$scope.isBusy=true;
    		CommonService.getData(false,'sweb/blogRest/getAllBlogs/'+searchObject.q+'/'+startRecord+'/'+8,{}).then(function(response){
    			
    		
    			if ((response.length+1)<10){
        			//document.getElementById("blogsLoadMore").innerHTML  = "No More Record To Show";
        			}
        			
        		
        		response.forEach(function(profile){
        			
        			profile.content=removeImageTag(profile.content);
        			
        			
        			if(!profile.imageUrl){
        				profile.imageUrl="img/blog/blog2.png";
        				
        			}
        			
        			blogController.blogs.push(profile);
        		
    			
    			  
    		
				
            
          	   
          	});
        		/*startRecord=startRecord+10;*/
        	}).finally(function(){
        		$scope.isBusy=false;
        		startRecord+=1;
        		 $scope.infiniteScroll = false;
        		blogController.loadingBlogs=false;
        	
        	});
    		
    	}
    		
    		
    
        

       
    }})();



