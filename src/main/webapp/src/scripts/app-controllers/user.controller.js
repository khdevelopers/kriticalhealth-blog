(function () {
    'use strict';

    angular
        .module('app')
        .controller('UserController', UserController);

    UserController.$inject = ['$location','CommonService','AuthenticationService', 'FlashService', '$rootScope','$scope','$auth','$http'];
    function UserController($location,CommonService,AuthenticationService,FlashService,$rootScope,$scope,$auth,$http) {
        var userController = this;
        
       
        
        $scope.alerts={};
        
        if($rootScope.globals.currentUser)
        {
        userController.userId=$rootScope.globals.currentUser.id;
        userController.firstName=$rootScope.globals.currentUser.firstName;
        userController.lastName=$rootScope.globals.currentUser.lastName;
        
        }
        userController.Save = Save;
        userController.flashsetinterval = flashsetinterval;
        userController.verifyCaptcha = verifyCaptcha;
       userController.user={}; 
   
       userController.onPersonaChange= onPersonaChange;
       userController.allowRegistration=allowRegistration;
       userController.authenticate = authenticate;
       userController.openSecondTab=openSecondTab;
       userController.isRegistrationAllowed=false;
       userController.disabled=false;
       userController.datasiteKey=data_site_key;
       userController.secretKey=secret_key;
       
       //userController.user.selectedPersonaId;
       var firstName=document.getElementById("first-name");
       var lastName=document.getElementById("last-name");
        (function initController() {
        	
			/* onchange function for EAV Set */
        	userController.personaMastersMap={}; 
        	userController.personaValue={}; 
        	
        	
        	
        	/*get personas from persona table where parent is equal to null*/
        	CommonService.getData(false,'sweb/personaMasterRest/getByParentId/'+-1,{}).then(function(response){
        	
    
				userController.personaMasters = response;
				userController.user.selectedPersonaId=userController.personaMasters[0].id;
				onPersonaChange(userController.personaMasters[0],-1);
        	});
        	allowRegistration();
        })();
        
        /*changing placeholder depending upon what user select in sighnup page*/
        $scope.changePlaceholder=function (){
        	if(userController.user.basePersonaId==1){
        	 	firstName.placeholder="COMPANY NAME";
        	}
        	else{
        		firstName.placeholder="FIRST NAME";
        	}
        };
    	function allowRegistration()
        {
    		
    	 CommonService.postData(false,'sweb/invitationRest/isRegistrationAllowed/','{"email":"'+($location.search().email==undefined?"":$location.search().email)+'","invitationKey":"'+$location.search().link+'"}',{}).then(function(iresponse){
    		
    			
    				userController.isRegistrationAllowed= iresponse;
    				var url=window.location.href;
		            var queryStart = url.indexOf("=") + 1,
		               queryEnd   = url.indexOf("&") +1 ,
		               query = url.slice(queryStart, queryEnd - 1);
		            if (queryStart>0 && queryEnd>0){
		            	userController.user.email=query;
		            	userController.disabled=true;
		            }
		            else if(queryStart==0){
		            	userController.disabled=false;
		            }
    			});
        }
    	
    	
    	
        function onPersonaChange(persona,index){
        	if(index!=-1)
    		{
    		for (var i = index+1; i < Object.keys(userController.personaMastersMap).length; i++) { 
    			 delete userController.personaMastersMap[i] ;
    		}
    		}
    		else{
    			userController.personaMastersMap={}; 
    		}
        	var parentId=persona!=undefined?persona.id:undefined;
        	if(parentId!=undefined && persona.leaf!=1)
			{
        		
        		CommonService.getData(false,'sweb/personaMasterRest/getByParentId/'+parentId,{}).then(function(response){
        
        		
        		
        		if(Object.keys(userController.personaMastersMap).length==0){
        		userController.personaMastersMap[0] = response;
        		}
        		else{
        		userController.personaMastersMap[Object.keys(userController.personaMastersMap).length] = response;
        		}
        		
        	});
			}
        }
        
        
      
        function openSecondTab()
        {
        	
        	
        	 
        
                 
                     
                	 $(".tab-paned").removeClass("active");
                     var radioitem = $('input[name="optradio"]:checked').val();
                     if(radioitem==1){
                    	 userController.firstName= "COMPANY NAME";
                    	 userController.showLastName=false;
                         $("#secondtab").delay(500).addClass("active");
                        
                     }else{
                    	 userController.firstName= "FIRST NAME";
                    	 userController.showLastName=true;
                         $("#secondtab").addClass("active");
                     }
                     $(".social-contn").show("1000");
                     return false;
                 
             
        }
        
   
        
        
        /*register user*/
        function Save() {
        	
        	var status=verifyCaptcha();
        	if(status==true)
        		{
        		if(userController.user.confirmPassword==userController.user.password)
            		{

                	
                    userController.dataLoading = true;
                    
                    if (userController.user.lastName==undefined || userController.user.lastName==null){
                    	userController.user.lastName="";
                    }
                    if( Object.keys(userController.personaMastersMap).length>0)
                    	{
                    	userController.user.personaId=userController.personaValue[Object.keys(userController.personaMastersMap).length-1].id;
                    	}
                    else{
                    	userController.user.personaId=userController.user.basePersonaId;
                    }
                    userController.user.registrationDate = new Date();
                    CommonService.postData(false, 'sweb/userRest/register', userController.user).then(function (response) {
                    	
                        if (response.success) {
                 
                        	 response = { success: true,message:'Registered Successfully' };
                             $location.path('/login');
                             FlashService.Success('Registered Successfully',true);
                             userController.user={};
                             $location.path('/regsuccess');
                        	
                        } else {
                       	 	    response.messages.forEach(function(singleMessage) {
                       	 	     //FlashService.Error(singleMessage.message);
                       	 	 $scope.alerts['passwordError']=[];
             				$scope.alerts['passwordError'] = [{ type: 'danger', createdAt: Date.now() , msg:  singleMessage.message }];
             				flashsetinterval($scope.alerts['passwordError']);
                       	 	 grecaptcha.reset();
                       	 
                       	 	    });
                         
                       	      userController.dataLoading = false;
                       }
                    });
                
                
            		}
            	else{
            		$scope.alerts['passwordError']=[];
    				$scope.alerts['passwordError'] = [{ type: 'danger', createdAt: Date.now() , msg:  password_matching_error }];
    				flashsetinterval($scope.alerts['passwordError']);
    			 	 grecaptcha.reset();
    				return false;
            	}
            	
            
        		}
        	else
        		{
        		$scope.alerts['passwordError']=[];
				$scope.alerts['passwordError'] = [{ type: 'danger', createdAt: Date.now() , msg:  verify_captcha }];
				flashsetinterval($scope.alerts['passwordError']);
			 	 grecaptcha.reset();
				return false;
        		}





        };
        
        
        /*flash messages interval*/
        function flashsetinterval(array)
        {
        	var maxLifespan = 5000;
    		// check once per second
    		var interval =setInterval(function checkItems(){
    		
    			array.forEach(function(item){
    				var d= convertUtcToLocalTime(Date.now())
    				var d1= convertUtcToLocalTime(item.createdAt)
    				var diff= Math.abs(d-d1);
    		        if(diff > maxLifespan){
    		        	array.shift(); 
    		        	
    		        	 $scope.$apply();   // remove first item
    		        	
    		        	
    		        	 clearInterval(interval);
    		        	
    		        }
    		    });
    		},1000);
        }
        
        
        /*verify captcha*/
        function verifyCaptcha()
        {	
        	var response = $('textarea#g-recaptcha-response').val();
        	
        	if(response!= null && response!= undefined && response!="")
        		{
        		return true;
        		}
        	else
        		{
        		return false;
        		}
        	
        }

     /* social  sign up   */
        /*for  google and facelogin*/
        function authenticate(provider){
        	userController.personaId=userController.personaValue[Object.keys(userController.personaMastersMap).length-1].id;
        	
            $auth.authenticate(provider, {personaId: userController.personaId})
              .then(function(response) {
            	  
            	  localStorage.setItem("access_token",response.data.tokenObject.access_token);
            	  
            	  CommonService.postData(true,'sweb/profileRest/getProfileByEmailId', '{"email":"'+response.data.id+'"}',{}).then(function(iresponse){
            	  
            	 
							    var innerResponse=iresponse;
								var mydate = new Date(innerResponse.dob);
								innerResponse.dob=mydate;
                            
								/* pictureView */
								if (innerResponse.profileVo.currentProfilePictureId != null) {
									innerResponse.profilePicture = projectUrl
											+ 'sweb/pictureRest/viewPicture/'
											+ innerResponse.profileVo.currentProfilePictureId;
									innerResponse.profilePicture_70_70= getPictureUrlWithExtension(innerResponse.profileVo.currentProfilePictureId,innerResponse.picType,innerResponse.picName,"thumnail_70_70");
									innerResponse.profilePicture_300_300= getPictureUrlWithExtension(innerResponse.profileVo.currentProfilePictureId,innerResponse.picType,innerResponse.picName,"thumnail_300_300");
									
								} else
									innerResponse.profilePicture = "img/defaultprofile.png";
								/* pictureView */
								
								
								$location.path('/'+innerResponse.userName);
								
							
							});
            	  
            	  // call login api
              })
              .catch(function(error) {
                if (error.data.error=="unauthorized") {
                	
                	$scope.alerts['passwordError']=[];
    				$scope.alerts['passwordError'] = [{ type: 'danger', createdAt: Date.now() , msg: error.data.error_description}];
    				flashsetinterval($scope.alerts['passwordError']);
                  // Popup error - invalid redirect_uri, pressed cancel button, etc.
                	/*FlashService.Error('User is already registered !!!');*/
                } else if (error.data) {
                  // HTTP response error from server
                	$scope.alerts['passwordError']=[];
    				$scope.alerts['passwordError'] = [{ type: 'danger', createdAt: Date.now() , msg: error.data.error_description}];
    				flashsetinterval($scope.alerts['passwordError']);
                } else {
                	FlashService.Error(error);
                }
              });
          };
          

    }})();

