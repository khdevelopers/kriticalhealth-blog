﻿(function () {
	'use strict';

	angular
	.module('app')
	.controller('LoginController', LoginController);

	LoginController.$inject = ['$location', 'AuthenticationService', 'FlashService','$scope','$cookieStore','$rootScope','$auth','CommonService', '$window'];
	function LoginController($location, AuthenticationService, FlashService,$scope,$cookieStore,$rootScope,$auth,CommonService, $window) {
		var loginController = this;
		$rootScope.resetPasswordInfo = {};
		loginController.user = {};
		loginController.login = login;
		loginController.loginOauth = loginOauth;
		/*loginController.connectFacebook=connectFacebook;*/
		loginController.authenticate = authenticate;
		var rootapp = '';
		if ( $location.search().hasOwnProperty( 'rootapp' ) ) {
			rootapp = $location.search()['rootapp'];
			   
		}
		
		(function initController() {			
			// reset login status
			//  AuthenticationService.ClearCredentials();
		})();
		//country list

		
		//status

		$scope.status = function(){
			if(isNaN($scope.emailOrMobile)){
				loginController.user.email = $scope.emailOrMobile;
				loginController.user.mobile = undefined;
			}else{
				loginController.user.mobile = $scope.emailOrMobile;
				loginController.user.email = undefined;	
			}
			CommonService.postData(false,'sweb/userRest/userStatus', loginController.user).then(function(response){

				if (response.success) {
					if(response.object == 1){
						$scope.showOtpBox = true;
						$scope.showPasswordBox= false;
						$scope.showUserNullBox= false;
						$scope.showOtpResetBox = false;
					}else if(response.object == 2){
						$scope.showPasswordBox= false;
						$scope.showUserNullBox= false;
						$scope.showOtpBox = false;
						$scope.showOtpResetBox = true;
					}else if(response.object == 3){
						$scope.showPasswordBox= true;
						$scope.showUserNullBox= false;
						$scope.showOtpBox = false;
						$scope.showOtpResetBox = false;

					}else if(response.object == 0){
						$scope.showUserNullBox= true;
						$scope.showOtpBox = false;
						$scope.showPasswordBox= false;
						$scope.showOtpResetBox = false;
					}



				}else{

					
					FlashService.Error('You are not Registered with us');

				}
			});
		}

		$scope.confirmOTP = function(){


			CommonService.postData(false,'sweb/userRest/confirmEmail', loginController.user).then(function(response){

				if (response) {
					if($scope.showOtpResetBox == true){
						$rootScope.resetPasswordInfo = loginController.user; 
						$location.path("/resetPassword");
					}
					
					$scope.status();
					FlashService.Success("OTP is Comfirm..!! Ready To Go");

				}else{

					
					

				}
			});

		}

		/*login method with spring security outh2.0 */
		function loginOauth() {
			if(isNaN($scope.emailOrMobile)){
				loginController.user.email = $scope.emailOrMobile;
				loginController.user.mobile = undefined;
			}else{
				loginController.user.mobile = $scope.emailOrMobile;
				loginController.user.email = undefined;	
			}
			loginController.dataLoading = true;
			//alert(loginController.user.userName);
			if(loginController.user.email != null || loginController.user.email != undefined ){
				var authtype = "email";

			}else if(loginController.user.mobile != null || loginController.user.mobile != undefined){

				var authtype = "mobile";
			}

			AuthenticationService.LoginOauth(loginController.user,authtype, rootapp, function (response) {

				if (response.success) {

							
					
					localStorage.setItem("access_token",response.object.access_token);
					CommonService.postData(true,'sweb/profileRest/getProfileByEmailId', loginController.user,{}).then(function(iresponse){

						var innerResponse=iresponse;
						var mydate = new Date(innerResponse.dob);
						innerResponse.dob=mydate;

						/* pictureView */
						if (innerResponse.profileVo.currentProfilePictureId != null) {
							innerResponse.profilePicture_70_70 =  getPictureUrlWithExtension(innerResponse.profileVo.currentProfilePictureId,'profile',innerResponse.profileVo.profilePictureFileName,'thumnail_70_70');
							innerResponse.profilePicture_300_300 =  getPictureUrlWithExtension(innerResponse.profileVo.currentProfilePictureId,'profile',innerResponse.profileVo.profilePictureFileName,'thumnail_300_300');
						} else {



							if(innerResponse.profileVo.template=="company"){
								innerResponse.profilePicture = "img/CompanyDefaultPic.png";
								innerResponse.profilePicture_70_70 = "img/CompanyDefaultPic.png";
								innerResponse.profilePicture_300_300 ="img/CompanyDefaultPic.png";

							}else{
								innerResponse.profilePicture = "img/defaultprofile.png";
								innerResponse.profilePicture_70_70 = "img/CompanyDefaultPic.png";
								innerResponse.profilePicture_300_300 ="img/CompanyDefaultPic.png";

							}
						}

						/* pictureView */
						AuthenticationService.SetCredentials(innerResponse.firstName,innerResponse.lastName, innerResponse.password, innerResponse.id,innerResponse,response.object.access_token);

						
						
						loginController.user={};
						
						$location.path('/blog-author');
						


					});


				} else {
					FlashService.Error(response.message);
					loginController.dataLoading = false;
				}
			});
		};

		/*    function connectFacebook()
        {
        	window.location = "http://localhost:9082/kriticalhealthcore/facebook";
        	 //return $http.redirect('http://localhost:9082/kriticalhealthcore/facebook',user).then(handleSuccess, handleError('Error getting user by username'));
        }
		 */


		/*login method for special logins e.g. google,facebook */
		function authenticate(provider){

			$auth.authenticate(provider, [])
			.then(function(response) {
				localStorage.setItem("access_token",response.data.tokenObject.access_token);



				if(response.data.isMyApiLogin && !response.data.hasRefreshToken){

					authenticate("google_api_login");
				}else{

					CommonService.postData(true,'sweb/profileRest/getProfileByEmailId', '{"email":"'+response.data.id+'"}',{}).then(function(iresponse){

						var innerResponse=iresponse;
						var mydate = new Date(innerResponse.dob);
						innerResponse.dob=mydate;







						/* pictureView */
						if (innerResponse.profileVo.currentProfilePictureId != null) {
							innerResponse.profilePicture_70_70 =  getPictureUrlWithExtension(innerResponse.profileVo.currentProfilePictureId,'profile',innerResponse.profileVo.profilePictureFileName,'thumnail_70_70');
							innerResponse.profilePicture_300_300 =  getPictureUrlWithExtension(innerResponse.profileVo.currentProfilePictureId,'profile',innerResponse.profileVo.profilePictureFileName,'thumnail_300_300');
						} else
						{



							if(innerResponse.profileVo.template=="company"){
								innerResponse.profilePicture = "img/CompanyDefaultPic.png";
								innerResponse.profilePicture_70_70 = "img/CompanyDefaultPic.png";
								innerResponse.profilePicture_300_300 ="img/CompanyDefaultPic.png";

							}else{
								innerResponse.profilePicture = "img/defaultprofile.png";
								innerResponse.profilePicture_70_70 = "img/CompanyDefaultPic.png";
								innerResponse.profilePicture_300_300 ="img/CompanyDefaultPic.png";

							}
						}
						/* pictureView */
						AuthenticationService.SetCredentials(innerResponse.firstName,innerResponse.lastName, innerResponse.password, innerResponse.id,innerResponse,response.data.tokenObject.access_token);
						
						
						
						
						
					

						$location.path('/download-app');


					});

				}




				// call login api
			})
			.catch(function(error) {
				if (error.data.error_description=="USER_IS_NOT_REGISTERED") {
					// Popup error - invalid redirect_uri, pressed cancel button, etc.
					FlashService.Error('You are not Registered with us');
				}
				else if (error.data.error_description=="USER_IS_REGISTERED") {
					// Popup error - invalid redirect_uri, pressed cancel button, etc.
					FlashService.Error('You already have an account');
				}
				else if(error.data.error_description=="YOUR_ACCOUNT_HAS_BEEN_DEACTIVATED"){
					FlashService.Error('Your Account Has Been Deactivated Contact Administrator for Help');

				}

				else if (error.data) {
					// HTTP response error from server
					FlashService.Error(error.data.message, error.status);
				} else {
					FlashService.Error(error);
				}
			});
		};



	}

})();
