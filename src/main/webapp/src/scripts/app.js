﻿(function () {
    'use strict';

  
    
     /*dependency injections for angularjs */
    /*define your own directives in filter.js and define here*/
    var app=  angular
        .module('app', ['ngResource', 'ngMessages','ui.router','ngRoute','ngSanitize','appFilters','ngCookies','infinite-scroll','fupApp','ngMaterial','CKEditorExample','ngAnimate','ui.bootstrap','dateParser','angular-loading-bar','HIGHERTHAN','YEARDROPDOWN','satellizer','xeditable', 'toastr' ,'vsGoogleAutocomplete','ngMap','numbersOnly','HEAD','summernote']);
  
        app.config(config)
        .run(run);

    
    
        
  

    
        /* dependency injections for config function */
    config.$inject = ['$routeProvider', '$locationProvider','$mdThemingProvider','cfpLoadingBarProvider','$authProvider'];
    
    
    /*config function for application*/
    function config($routeProvider, $locationProvider,$mdThemingProvider,cfpLoadingBarProvider,$authProvider) {
       
    	/*threshhold latency for loading bar */
    	/*define delay of loading bar after xhr request is send */
    	cfpLoadingBarProvider.latencyThreshold = 100;
        
    	
    
    	/*depending on the url the route will redirect to a particular html  */
    	/*define route variable and route params here to use in your view and controller  */
        $routeProvider
            .when('/', {
                templateUrl: 'home.html?v=1.4',
                
                
               
                	
            })
            .when('/home', {
                templateUrl: 'home.html?v=1.4',
                
                
               
                	
            })
            .when('/regsuccess', {
                templateUrl: 'regsuccess.html'
            })
             .when('/forgotPassword', {
                templateUrl: 'forgotPassword.html'
            })
             .when('/confirmEmail', {
                templateUrl: 'confirmEmail.html'
            })
             .when('/resetPassword', {
                templateUrl: 'resetPassword.html'
            })
              .when('/changePassword', {
                templateUrl: 'changePassword.html'
           })
            .when('/blog', {
            
                templateUrl: 'blogs.html?v=1.0'
                	
               })
               
                .when('/Popular-Blogs', {
            	isBlog:true,
                templateUrl: 'PopularBlog.html?v=1.0'
                	
               })
               .when('/blog-edit/:blogId', {
            	isBlog:true,
            	templateUrl: 'postBlog.html?v=1.3'
            })
             .when('/blog-create', {
            	isBlog:true,
            	templateUrl: 'postBlog.html?v=1.1'
            })
           
                  .when('/blog-inner/:id', {
            	isBlog:true,
                templateUrl: 'blog-inner.html?v=1.3'
                	
            })
             .when('/blog-author/:userId', {
        	 isBlog:true,
                templateUrl: 'blog-author.html?v=1.2'
                	
            })
            
            .when('/blog-author', {
            	isBlog:true,
            	templateUrl: 'blog-author.html?v=1.3'
            })
                          .when('/success', {
                templateUrl: 'success.html',
              
            })
            .when('/failure', {
                templateUrl: 'failure.html',
              
            })
              .when('/view-app', {
                templateUrl: 'view-app.html',
              
            })
             .when('/suggestion', {
                templateUrl: 'suggestion.html',
              
            })
            
            
            
            
            
              
            .when('/login', {
                templateUrl: 'login.html'
            })
             .when('/aboutus', {
                templateUrl: 'AboutUs.html'
            })
            
            .when('/pageNotFound', {
                templateUrl: 'pageNotFound.html'
            })
              .when('/deactivated', {
                templateUrl: 'deactivated.html'
            })
            
            
             
            
            
            .otherwise({ redirectTo: '/home' });
        
        
       
    	/*for removal of # symbol from url  */
        $locationProvider.html5Mode({
        	  enabled: true,
        	 
        	});
        
       
        /*takes client id from config.js */
        $authProvider.facebook({
            clientId: facebook_client_id
        });
        
        $authProvider.google({
            clientId: google_client_id
          });
        
        
    }

   
    /*dependency injection for application run function*/
    run.$inject = ['$rootScope', '$location', '$http','$cookieStore','editableOptions','$route'];
    
    
    function getCookie(key) {
		var list = [];
		var all = document.cookie;
		if (all) {
			list = all.split('; ');
		}

		var cookies = {};
		var hasCookies = false;

		for (var i = 0; i < list.length; ++i) {
			if (list[i]) {
				var cookie = list[i];
				var pos = cookie.indexOf('=');
				var name = cookie.substring(0, pos);
				var value = cookie.substring(pos + 1);
				if (angular.isUndefined(value))
					continue;

				if (key === undefined || key === name) {
					try {
						cookies[name] = JSON.parse(value);
					} catch (e) {
						cookies[name] = value;
					}
					if (key === name) {
						return cookies[name];
					}
					hasCookies = true;
				}
			}
		}
		if (hasCookies && key === undefined) {
			return cookies;
		}
	}
   
    
    /*application run function*/
    function run($rootScope, $location,  $http,$cookieStore,editableOptions,$route) {
    	editableOptions.theme = 'bs3';
        // keep user logged in after page refresh
    	

		var loggedIn = getCookie('access_token');
		
		
		
		
		if (loggedIn) {
			//$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
			localStorage.setItem("access_token", getCookie('access_token'));
									
		} else {
			loggedIn = localStorage.access_token;
		}
		
    	
  
       
		/*routescope global variable declaration*/
    	$rootScope.globals = getCookie('globals') || {};
    	//$rootScope.globals = $cookieStore.get('globals') || {};
       
    	$rootScope.profileDetails = $cookieStore.get('profileDetails') || {};
    	
    	
    	if(loggedIn && ($location.path()=='/home' || $location.path()=='/')){
			$location.path('/download-app');
		}
    	
    
      

        
        /*fires on every location change*/
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
        	
        	
        	
        	 loggedIn = getCookie('access_token') && getCookie('globals') ;
        	
       /*add pages that is restricted without login*/
        	
        	 
        	 
        	 
        	 
        	
        	
            var nonRestrictedPage=true;
            $.each([ '/changePassword', 
                    
                    ,'/view-app','/download-app','/success'
                     ], function(index, value){            /*   if($location.path().includes(value))*/
            	
            	if($location.path()==value)
            	   {
            	   nonRestrictedPage=false;
            	   }
            });
            

            $('#loginModal').modal('hide');

			 $('.modal-backdrop').hide();
	    	 $("body").css("overflow","auto");
            
          
            
            /*for these url headers are not included*/
            if(($location.path()=='/home' || $location.path()=='/')||($location.path()=='/forgotPassword')||($location.path()=='/resetPassword') ||($location.path()=='/confirmEmail') ||  ($location.path()=='/regsuccess'|| ($location.path()=='/invitation') || ! loggedIn   ))
      		{
        
           $rootScope.shouldHeaderBeIncluded=false;
           $rootScope.loginBodyIncluded='startpage';
      		}
      	else{
      		$rootScope.shouldHeaderBeIncluded=true;
      		$rootScope.loginBodyIncluded='kh';
      	 } 
            

            
            
            /*if it is a non restricted page and user is not logged in */
            /*user will be redirected to login page  */
        if (!nonRestrictedPage && !loggedIn) {
        	 $location.path('/home');
        	 $route.reload();
        
            }
        });
    }
    

  
})();