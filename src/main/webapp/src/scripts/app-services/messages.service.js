

(function () {
    'use strict';
   var basePath="";
 
    angular
        .module('app')
        .factory('MessagesService', MessagesService);

    MessagesService.$inject = ['$http'];

    function MessagesService($http) {
        var service = {};
        service.save=save;
        service.getAllMessageUsers=getAllMessageUsers;
        service.getConversations=getConversations;
        
        service.getNoOfMessages=getNoOfMessages;
        return service;
        


        
        
        function save(messages,callback) {
        	var response= $http.post(projectUrl+'sweb/messagesRest/save',messages).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
          	  var response;
             
          	  if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,message:"message required"};
              }
              callback(restResponse);
              return response;
             
        	
          });
           
        }
        
        function getNoOfMessages(receiverId,senderId,callback) {
        	var response= $http.get(projectUrl+'sweb/messagesRest/getNoOfMessages/' + receiverId+'/'+senderId).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }  
        
        
        
        
        function getAllMessageUsers(userId,firstResult, maxResult,callback) {
        	var response= $http.get(projectUrl+'sweb/messagesRest/getMessangersVoByUserId/'+userId+'/'+firstResult+'/'+maxResult).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
        		ignoreLoadingBar: true;
              callback(restResponse);
          });
        	
            return response;
        }
        
        function getConversations(receiverId,senderId,firstResult, maxResult,callback) {
        	var response= $http.get(projectUrl+'sweb/messagesRest/getConversations/'+receiverId+'/'+senderId+'/'+firstResult+'/'+maxResult).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
        		ignoreLoadingBar: true;
              callback(restResponse);
          });
        	
            return response;
        }

        

        function handleSuccess(data) {
            return data.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        };//handle error end
    };//eavAttributeService end

})();
