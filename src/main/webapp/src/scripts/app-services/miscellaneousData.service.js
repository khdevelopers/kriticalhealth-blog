

(function () {
    'use strict';
   var basePath="";
 
    angular
        .module('app')
        .factory('MiscellaneousDataService', MiscellaneousDataService);

    MiscellaneousDataService.$inject = ['$http'];

    function MiscellaneousDataService($http) {
        var service = {};

        
        service.save=save;
        service.update=update;
        service.getMiscellaneousDataByType=getMiscellaneousDataByType;
     
    
        
        return service;

        
        
        function save(miscellaneousData,callback) {
        	var response= $http.post(projectUrl+'sweb/miscellaneousDataRest/save',miscellaneousData).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
          	  var response;
             
          	  if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }
              callback(response);
              return response;
        	
          });
           
        }
        
        function update(miscellaneousData,callback){
        	var restResponse= $http.put(projectUrl+'sweb/miscellaneousDataRest/update',miscellaneousData).then(handleSuccess, handleError('Error')).then(function (restResponse) {
          	  var response;
              if (restResponse.success) {
                  response = { success: true ,message:" updated Succesfully"};
              } else {
            	  
                  response = { success: false ,message:" Error Editing"};
              }
             callback(restResponse);
        	
              return restResponse;
          });
           
         }// update end
              
        //old
        function getMiscellaneousDataByType (miscellaneousDataType,referenceId,callback) {
        	var response= $http.post(projectUrl+'sweb/miscellaneousDataRest/getMiscellaneousDataByType/' + referenceId,miscellaneousDataType).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }

       

        function handleSuccess(data) {
            return data.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        };//handle error end
    };//eavAttributeService end

})();
