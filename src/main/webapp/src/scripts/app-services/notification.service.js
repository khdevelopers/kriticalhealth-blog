

(function () {
    'use strict';
   var basePath="";
 
    angular
        .module('app')
        .factory('NotificationService', NotificationService);

    NotificationService.$inject = ['$http'];

    function NotificationService($http) {
        var service = {};

        service.getNotificationByUserId = getNotificationByUserId;
        
        service.getAllNotificationsByUserId = getAllNotificationsByUserId;
        
        service.updateNotification = updateNotification;
        
        service.getTotalNoOfNotifications=getTotalNoOfNotifications;
        service.updateMessages=updateMessages;
        service.getNoOfMessageNotificationUsers=getNoOfMessageNotificationUsers;
        service.getMessagesByUserId=getMessagesByUserId;
        service.getFriendRequestCount=getFriendRequestCount;
        service.updateFriendRequests=updateFriendRequests;
        service. getFriendRequestNotification=getFriendRequestNotification;
        
        
        
        return service;


        function getNotificationByUserId(userId,callback) {
        	var response= $http.get(projectUrl+'sweb/notificationRest/getNotificationCount/' + userId).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }
        
        function getTotalNoOfNotifications(userId,callback) {
        	var response= $http.get(projectUrl+'sweb/notificationRest/getTotalNoOfNotifications/' + userId).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }
        
        function getFriendRequestCount(userId,callback) {
        	var response= $http.get(projectUrl+'sweb/notificationRest/getFriendRequestCount/' + userId).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }



        function getAllNotificationsByUserId(userId,firstResult,maxResult,callback) {
        	var response= $http.get(projectUrl+'sweb/notificationRest/getNotificationVos/' + userId+'/'+firstResult+'/'+maxResult).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }
        
        
        function getFriendRequestNotification(userId,firstResult,maxResult,callback) {
        	var response= $http.get(projectUrl+'sweb/notificationRest/getFriendRequestNotification/' + userId+'/'+firstResult+'/'+maxResult).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }
        
        
        function getMessagesByUserId(userId,firstResult,maxResult,callback) {
        	var response= $http.get(projectUrl+'sweb/messagesRest/getMessagesByUserId/' + userId+'/'+firstResult+'/'+maxResult).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }
        
        
        
        function updateNotification(userId,callback) {
        	var response= $http.get(projectUrl+'sweb/notificationRest/updateNotification/' + userId).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }
        
        function updateFriendRequests(userId,callback) {
        	var response= $http.get(projectUrl+'sweb/notificationRest/updateFriendRequests/' + userId).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }
        
        
        
        
        
        function updateMessages(userId,callback) {
        	var response= $http.get(projectUrl+'sweb/messagesRest/updateMessages/' + userId).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }
        
        function getNoOfMessageNotificationUsers(userId,callback) {
        	var response= $http.get(projectUrl+'sweb/messagesRest/getNoOfMessageNotificationUsers/' + userId).then(handleSuccess, handleError('Error getting user profile')).then(function (restResponse) {
              callback(restResponse);
          });
        	
            return response;
        }
        

       

        function handleSuccess(data) {
            return data.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        };//handle error end
    };//eavAttributeService end

})();
