

(function () {
    'use strict';
   var basePath="";
 
    angular
        .module('app')
        .factory('EavAttributeSetService', EavAttributeSetService);

    EavAttributeSetService.$inject = ['$http'];
  //  document.write('<scr'+'ipt type="text/javascript" src="config.js" ></scr'+'ipt>');

    function EavAttributeSetService($http) {
        var service = {};

        
        service.save=save;
        service.search=search;
        service.update=update;
        service.count=count;
        service.get=get;
        service.getCompleteTree=getCompleteTree;
 
        return service;
        //document.write('<script type="text/javascript" src="config.js"></script>');
        function save(eavAttributeSet,callback) {
        	var response= $http.post(projectUrl+'sweb/eavAttributeSetRest/save',eavAttributeSet).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
          	  var response;
             
          	  if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }
              callback(response);
              return response;
        	
          });
           
        }
        
        function search(eavAttributeSet,firstResult, maxResult,callback){
        	var restResponse= $http.post(projectUrl+'sweb/eavAttributeSetRest/search/'+firstResult+'/'+maxResult,eavAttributeSet).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
          	 var response;
              if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
        	
        	
        }
        
        
        function count(eavAttributeSet,callback){
        	
        	$http.post(projectUrl+'sweb/eavAttributeSetRest/count',eavAttributeSet).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
        		 callback(restResponse);
             });;
        }
       
        
        function get(eavAttributeSet,id,callback){
        	var restResponse= $http.get(projectUrl+'sweb/eavAttributeSetRest/get/'+id,eavAttributeSet).then(handleSuccess, handleError('Error')).then(function (restResponse) {
          	/*  var response;
              if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }*/
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
         }//get end
        
        
        function getCompleteTree(eavAttributeSetId,callback){
        	var restResponse= $http.get(projectUrl+'sweb/eavAttributeSetRest/getCompleteTree/'+eavAttributeSetId).then(handleSuccess, handleError('Error')).then(function (restResponse) {
          	/*  var response;
              if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }*/
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
         }//get end
        
        function update(eavAttributeSet,callback){
        	var restResponse= $http.put(projectUrl+'sweb/eavAttributeSetRest/update',eavAttributeSet).then(handleSuccess, handleError('Error')).then(function (restResponse) {
          	  var response;
              if (restResponse.success) {
                  response = { success: true ,message:" Edit Succesfully"};
              } else {
            	  
                  response = { success: false ,message:" Error Editing"};
              }
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
         }// update end
        
        
         
        
        
        
        

       

        function handleSuccess(data) {
            return data.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        };//handle error end
    };//eavAttributeService end

})();
