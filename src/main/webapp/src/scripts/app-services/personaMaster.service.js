

(function () {
    'use strict';
   var basePath="";
 
    angular
        .module('app')
        .factory('PersonaMasterService', PersonaMasterService);

    PersonaMasterService.$inject = ['$http'];
  //  document.write('<scr'+'ipt type="text/javascript" src="config.js" ></scr'+'ipt>');

    function PersonaMasterService($http) {
        var service = {};

        
       
        service.search=search;
        service.get=get;
      /*  service.specialityAutosuggest= specialityAutosuggest;*/
        return service;
       
        function search(parentId,callback){
        	var restResponse= $http.get(projectUrl+'sweb/personaMasterRest/getByParentId/'+parentId).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
          	 var response;
              if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
        	
        	
        }
        
        function get(id,callback){
        	var restResponse= $http.get(projectUrl+'sweb/personaMasterRest/get/'+id).then(handleSuccess, handleError('Error')).then(function (restResponse) {
          	/*  var response;
              if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }*/
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
         }//get end
        
/*        function specialityAutosuggest(query,parentId) {

			var returnHtml = {};
			
			var data = { "name": query , "parentId": parentId};
			
			jQuery.ajax({
				url : projectUrl + 'sweb/personaMasterRest/search/'
						+ 0 + '/' + 10,
				async : false,
				cache : false,
				type: 'POST',
				dataType : "json",
				data: data,
				success : function(html) {
					returnHtml = html;
				}
			});
			return returnHtml;
		}
       
        */
        
        
        

        function handleSuccess(data) {
            return data.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        };//handle error end
    };//eavAttributeService end

})();
