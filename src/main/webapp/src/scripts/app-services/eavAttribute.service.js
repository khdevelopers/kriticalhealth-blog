

(function () {
    'use strict';
   var basePath="";
 
    angular
        .module('app')
        .factory('EavAttributeService', EavAttributeService);

    EavAttributeService.$inject = ['$http'];
  //  document.write('<scr'+'ipt type="text/javascript" src="config.js" ></scr'+'ipt>');

    function EavAttributeService($http) {
        var service = {};

        
        service.save=save;
        service.search=search;
        service.update=update;
        service.count=count;
        service.get=get;

        return service;
        //document.write('<script type="text/javascript" src="config.js"></script>');
        function save(eavAttribute,callback) {
        	var response= $http.post(projectUrl+'sweb/eavAttributeRest/save',eavAttribute).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
          	  var response;
             
          	  if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }
              callback(response);
              return response;
        	
          });
           
        }
        
        function search(eavAttribute,firstResult, maxResult,callback){
        	var restResponse= $http.post(projectUrl+'sweb/eavAttributeRest/search/'+firstResult+'/'+maxResult,eavAttribute).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
          	 var response;
              if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
        	
        	
        }
        
        
        function count(eavAttribute,callback){
        	
        	$http.post(projectUrl+'sweb/eavAttributeRest/count',eavAttribute).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
        		 callback(restResponse);
             });;
        }
       
        
        function get(eavAttribute,id,callback){
        	var restResponse= $http.get(projectUrl+'sweb/eavAttributeRest/get/'+id,eavAttribute).then(handleSuccess, handleError('Error')).then(function (restResponse) {
          	/*  var response;
              if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }*/
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
         }//get end
        
        function update(eavAttribute,callback){
        	var restResponse= $http.put(projectUrl+'sweb/eavAttributeRest/update',eavAttribute).then(handleSuccess, handleError('Error')).then(function (restResponse) {
          	  var response;
              if (restResponse.success) {
                  response = { success: true ,message:" Edit Succesfully"};
              } else {
            	  
                  response = { success: false ,message:" Error Editing"};
              }
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
         }// update end
        
        
         
        
        
        
        

       

        function handleSuccess(data) {
            return data.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        };//handle error end
    };//eavAttributeService end

})();
