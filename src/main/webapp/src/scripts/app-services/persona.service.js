(function () {
    'use strict';
   var basePath="";
    angular
        .module('app')
        .factory('PersonaService', PersonaService);

    PersonaService.$inject = ['$http'];
    function PersonaService($http) {
        var service = {};

        
        service.save=save;

        return service;

        function save(persona,callback) {
        	var response= $http.post(projectUrl+'sweb/personaRest/save',persona).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
          	  var response;
             
          	  if (restResponse.success) {
          	
          		  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,message:""};
              } 
              callback(response);
              
             
          });
           
        }

       

        function handleSuccess(data) {
            return data.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        };
    };

})();
