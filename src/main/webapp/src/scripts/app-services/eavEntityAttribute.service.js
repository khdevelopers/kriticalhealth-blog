

(function () {
    'use strict';
   var basePath="";
 
    angular
        .module('app')
        .factory('EavEntityAttributeService', EavEntityAttributeService);

    EavEntityAttributeService.$inject = ['$http'];

    function EavEntityAttributeService($http) {
        var service = {};

        
        service.save=save;
        service.search=search;
        service.update=update;
        service.count=count;
        service.get=get;

        return service;
       
        function save(eavEntityAttribute,callback) {
        	var response= $http.post(projectUrl+'sweb/eavEntityAttributeRest/save',eavEntityAttribute).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
          	  var response;
             
          	  if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }
              callback(response);
              return response;
        	
          });
           
        }
        
        function search(eavEntityAttribute,firstResult, maxResult,callback){
        	var restResponse= $http.post(projectUrl+'sweb/eavEntityAttributeRest/search/'+firstResult+'/'+maxResult,eavEntityAttribute).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
          	 var response;
              if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
        	
        	
        }
        
        
        function count(eavEntityAttribute,callback){
        	
        	$http.post(projectUrl+'sweb/eavEntityAttributeRest/count',eavEntityAttribute).then(handleSuccess, handleError('Error getting user by username')).then(function (restResponse) {
        		 callback(restResponse);
             });;
        }
       
        
        function get(eavEntityAttribute,id,callback){
        	var restResponse= $http.get(projectUrl+'sweb/eavEntityAttributeRest/get/'+id,eavEntityAttribute).then(handleSuccess, handleError('Error')).then(function (restResponse) {
          	/*  var response;
              if (restResponse.success) {
                  response = { success: true ,message:"Saved Succesfully"};
              } else {
            	  
                  response = { success: false ,,message:"Attribute Code Should be Between Min-3 and Max-30"};
              }*/
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
         }//get end
        
        function update(eavEntityAttribute,callback){
        	var restResponse= $http.put(projectUrl+'sweb/eavEntityAttributeRest/update',eavEntityAttribute).then(handleSuccess, handleError('Error')).then(function (restResponse) {
          	  var response;
              if (restResponse.success) {
                  response = { success: true ,message:" Edit Succesfully"};
              } else {
            	  
                  response = { success: false ,message:" Error Editing"};
              }
             callback(restResponse);
        	 console.log(restResponse.toString());
              return restResponse;
          });
           
         }// update end
        
        
         
        
        
        
        

       

        function handleSuccess(data) {
            return data.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        };//handle error end
    };//eavAttributeService end

})();
