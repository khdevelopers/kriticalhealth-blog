// include gulp
var gulp = require('gulp'); 
var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');
var concat = require('gulp-concat');
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');
var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');

// include plug-ins
var jshint = require('gulp-jshint');

// JS hint task
//gulp.task('jshint', function() {
//  gulp.src('./src/scripts/**/*.js')
//    .pipe(jshint())
//    .pipe(jshint.reporter('default'));
//});

//minify new images
//gulp.task('imagemin', function() {
//  var imgSrc = './src/images/**/*',
//      imgDst = './build/images';
//  gulp.src(imgSrc)
//    .pipe(changed(imgDst))
//    .pipe(imagemin())
//    .pipe(gulp.dest(imgDst));
//});


//JS concat, strip debugging and minify
gulp.task('scripts', function() {
  gulp.src(['./src/scripts/app-services/valformaUtil.js','./src/scripts/app-services/filter.js','./src/scripts/js/common.messages.js','./src/scripts/app.js','./src/scripts/app-services/group.service.js','./src/scripts/app-services/authentication.service.js','./src/scripts/app-services/flash.service.js','./src/scripts/app-services/notification.service.js','./src/scripts/app-services/user.service.js','./src/scripts/app-services/eavAttribute.service.js','./src/scripts/app-services/persona.service.js','./src/scripts/app-services/profile.service.js','./src/scripts/app-services/profileexp.service.js','./src/scripts/app-services/activityStream.service.js','./src/scripts/app-services/activityStreamLike.service.js','./src/scripts/app-services/activityStreamComment.service.js','./src/scripts/app-services/blog.service.js','./src/scripts/app-services/fileUpload.service.js','./src/scripts/app-services/eavAttributeOption.service.js','./src/scripts/app-services/eavAttributeSet.service.js','./src/scripts/app-services/lookUpCodes.service.js','./src/scripts/app-services/lookUpType.service.js','./src/scripts/app-services/eavAttributeGroup.service.js','./src/scripts/app-services/eavEntityAttribute.service.js','./src/scripts/app-services/jobApplication.service.js','./src/scripts/app-services/jobs.service.js','./src/scripts/app-services/groupMember.service.js','./src/scripts/app-services/groupRequest.service.js','./src/scripts/app-services/privacySettings.service.js','./src/scripts/app-services/qualification.service.js','./src/scripts/app-services/personaMaster.service.js','./src/scripts/app-services/role.service.js','./src/scripts/app-services/roleFunctionJoin.service.js','./src/scripts/app-services/userRoleJoin.service.js','./src/scripts/app-services/appointments.service.js','./src/scripts/app-services/commentLike.service.js','./src/scripts/app-services/messages.service.js','./src/scripts/app-services/awards.service.js','./src/scripts/app-services/companySpecialization.service.js','./src/scripts/app-services/invitation.service.js','./src/scripts/app-services/miscellaneousData.service.js','./src/scripts/app-services/common.service.js','./src/scripts/app-controllers/postedJobs.controller.js','./src/scripts/app-controllers/invitation.controller.js','./src/scripts/app-controllers/header.controller.js','./src/scripts/app-controllers/Group.controller.js','./src/scripts/app-controllers/user.controller.js','./src/scripts/app-controllers/login.controller.js','./src/scripts/app-controllers/Follow.controller.js','./src/scripts/app-controllers/Friend.controller.js','./src/scripts/app-controllers/EavAttribute.controller.js','./src/scripts/app-controllers/Persona.controller.js','./src/scripts/app-controllers/profile.controller.js','./src/scripts/app-controllers/Profileexp.Controller.js','./src/scripts/app-controllers/lookUpCodes.controller.js','./src/scripts/app-controllers/lookUpType.controller.js','./src/scripts/app-controllers/lookUpTypeEdit.controller.js','./src/scripts/app-controllers/activityStream.controller.js','./src/scripts/app-controllers/blog.controller.js','./src/scripts/app-controllers/blogCreate.controller.js','./src/scripts/app-controllers/blogAuthor.controller.js','./src/scripts/app-controllers/blogInner.controller.js','./src/scripts/app-controllers/eavAttributeSet.controller.js','./src/scripts/app-controllers/eavAttributeGroup.controller.js','./src/scripts/app-controllers/jobs.controller.js','./src/scripts/app-controllers/network.controller.js','./src/scripts/app-controllers/privacySettings.controller.js','./src/scripts/app-controllers/profileSearch.controller.js','./src/scripts/app-controllers/groupSearch.controller.js','./src/scripts/app-controllers/userSearch.controller.js','./src/scripts/app-controllers/role.controller.js','./src/scripts/app-controllers/roleFunctionJoin.controller.js','./src/scripts/app-controllers/forgotPassword.controller.js','./src/scripts/app-controllers/confirmEmail.controller.js','./src/scripts/app-controllers/resetPassword.controller.js','./src/scripts/app-controllers/changePassword.controller.js','./src/scripts/app-controllers/userRoleJoin.controller.js','./src/scripts/app-controllers/notification.controller.js','./src/scripts/app-controllers/messages.controller.js','./src/scripts/app-controllers/groupProfile.controller.js','./src/scripts/app-controllers/groupMemberSearch.controller.js','./src/scripts/app-controllers/appointments.controller.js','./src/scripts/app-controllers/publicProfille.controller.js','./src/scripts/app-controllers/bugDetails.controller.js','./src/scripts/app-controllers/commonProfile.controller.js','./src/scripts/app-controllers/search.controller.js'])
    .pipe(concat('script.js'))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest('./build/scripts/'));
});

gulp.task('framework-scripts-local', function() {
  gulp.src(['./src/scripts/app-services/config_local.js','./src/scripts/js/jquery.min.js','./src/scripts/js/jquery-migrate-1.4.0.js','./src/scripts/js/jquery-ui.min.js','./src/scripts/js/bootstrap.min.js','./src/scripts/js/bootstrap-typeahead.min.js','./src/scripts/js/plugins/cropper/js/cropper.js','./src/scripts/js/angular.js','./src/scripts/js/angular-route.js','./src/scripts/js/angular-aria.min.js','./src/scripts/js/angular-animate.min.js','./src/scripts/js/angular-material.js','./src/scripts/js/angular-cookies.js','./src/scripts/js/loading-bar.js','./src/scripts/js/angular-messages.js','./src/scripts/js/angular-resource.js','./src/scripts/js/angular-sanitize.js','./src/scripts/js/angular-ui-router.js','./src/scripts/js/satellizer.min.js','./src/scripts/js/summernote.js','./src/scripts/js/bootbox.js','./src/scripts/js/instafilta.min.js','./src/scripts/js/ui-bootstrap-tpls-2.2.0.min.js','./src/scripts/js/xeditable.min.js','./src/scripts/js/angular-toastr.tpls.js','./src/scripts/js/ng-infinite-scroll.min.js','./src/scripts/js/google-places-autocomplete.js','./src/scripts/js/ng-map.min.js','./src/scripts/js/bootstrap-select.min.js'])
    .pipe(concat('frameworks.js'))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest('./build/scripts/'));
});

gulp.task('framework-scripts-aws', function() {
  gulp.src(['./src/scripts/app-services/config_aws.js','./src/scripts/js/jquery.min.js','./src/scripts/js/jquery-migrate-1.4.0.js','./src/scripts/js/jquery-ui.min.js','./src/scripts/js/bootstrap.min.js','./src/scripts/js/bootstrap-typeahead.min.js','./src/scripts/js/plugins/cropper/js/cropper.js','./src/scripts/js/angular.js','./src/scripts/js/angular-route.js','./src/scripts/js/angular-aria.min.js','./src/scripts/js/angular-animate.min.js','./src/scripts/js/angular-material.js','./src/scripts/js/angular-cookies.js','./src/scripts/js/loading-bar.js','./src/scripts/js/angular-messages.js','./src/scripts/js/angular-resource.js','./src/scripts/js/angular-sanitize.js','./src/scripts/js/angular-ui-router.js','./src/scripts/js/satellizer.min.js','./src/scripts/js/summernote.js','./src/scripts/js/bootbox.js','./src/scripts/js/instafilta.min.js','./src/scripts/js/ui-bootstrap-tpls-2.2.0.min.js','./src/scripts/js/xeditable.min.js','./src/scripts/js/angular-toastr.tpls.js','./src/scripts/js/ng-infinite-scroll.min.js','./src/scripts/js/google-places-autocomplete.js','./src/scripts/js/ng-map.min.js','./src/scripts/js/bootstrap-select.min.js'])
    .pipe(concat('frameworks-aws.js'))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest('./build/scripts/'));
});
gulp.task('framework-scripts-aws-test', function() {
  gulp.src(['./src/scripts/app-services/config_aws_test.js','./src/scripts/js/jquery.min.js','./src/scripts/js/jquery-migrate-1.4.0.js','./src/scripts/js/jquery-ui.min.js','./src/scripts/js/bootstrap.min.js','./src/scripts/js/bootstrap-typeahead.min.js','./src/scripts/js/plugins/cropper/js/cropper.js','./src/scripts/js/angular.js','./src/scripts/js/angular-route.js','./src/scripts/js/angular-aria.min.js','./src/scripts/js/angular-animate.min.js','./src/scripts/js/angular-material.js','./src/scripts/js/angular-cookies.js','./src/scripts/js/loading-bar.js','./src/scripts/js/angular-messages.js','./src/scripts/js/angular-resource.js','./src/scripts/js/angular-sanitize.js','./src/scripts/js/angular-ui-router.js','./src/scripts/js/satellizer.min.js','./src/scripts/js/summernote.js','./src/scripts/js/bootbox.js','./src/scripts/js/instafilta.min.js','./src/scripts/js/ui-bootstrap-tpls-2.2.0.min.js','./src/scripts/js/xeditable.min.js','./src/scripts/js/angular-toastr.tpls.js','./src/scripts/js/ng-infinite-scroll.min.js','./src/scripts/js/google-places-autocomplete.js','./src/scripts/js/ng-map.min.js','./src/scripts/js/bootstrap-select.min.js'])
    .pipe(concat('frameworks-aws-test.js'))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest('./build/scripts/'));
});
gulp.task('framework-scripts-demo', function() {
  gulp.src(['./src/scripts/app-services/config_demo.js','./src/scripts/js/jquery.min.js','./src/scripts/js/jquery-migrate-1.4.0.js','./src/scripts/js/jquery-ui.min.js','./src/scripts/js/bootstrap.min.js','./src/scripts/js/bootstrap-typeahead.min.js','./src/scripts/js/plugins/cropper/js/cropper.js','./src/scripts/js/angular.js','./src/scripts/js/angular-route.js','./src/scripts/js/angular-aria.min.js','./src/scripts/js/angular-animate.min.js','./src/scripts/js/angular-material.js','./src/scripts/js/angular-cookies.js','./src/scripts/js/loading-bar.js','./src/scripts/js/angular-messages.js','./src/scripts/js/angular-resource.js','./src/scripts/js/angular-sanitize.js','./src/scripts/js/angular-ui-router.js','./src/scripts/js/satellizer.min.js','./src/scripts/js/summernote.js','./src/scripts/js/bootbox.js','./src/scripts/js/instafilta.min.js','./src/scripts/js/ui-bootstrap-tpls-2.2.0.min.js','./src/scripts/js/xeditable.min.js','./src/scripts/js/angular-toastr.tpls.js','./src/scripts/js/ng-infinite-scroll.min.js','./src/scripts/js/google-places-autocomplete.js','./src/scripts/js/ng-map.min.js','./src/scripts/js/bootstrap-select.min.js'])
    .pipe(concat('frameworks-demo.js'))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest('./build/scripts/'));
});

gulp.task('framework-styles', function() {
	  gulp.src(['./src/styles/css/bootstrap.min.css','./src/styles/css/font-awesome.min.css','./src/styles/css/loading-bar.css','./src/styles/css/save.css','./src/styles/css/save-tab.css','./src/styles/css/wc-animate.css','./src/styles/css/summernote.css','./src/styles/css/style-tab.css','./src/styles/css/style-responsive.css','./src/styles/css/main.css','./src/styles/css/angular-material.css','./src/styles/css/xeditable.min.css','./src/styles/css/datepicker.css','./src/styles/css/angular-toastr.css'])
	    .pipe(concat('framework-styles.css'))
	    .pipe(autoprefix('last 2 versions'))
	    .pipe(minifyCSS())
	    .pipe(gulp.dest('./build/styles/'));
	});

gulp.task('styles', function() {
	  gulp.src(['./src/styles/css/style.css','./src/styles/css/valforma-custom.css'])
	    .pipe(concat('styles.css'))
	    .pipe(autoprefix('last 2 versions'))
	    .pipe(minifyCSS())
	    .pipe(gulp.dest('./build/styles/'));
	});


/*gulp.task('default', ['scripts', 'htmlpage', 'scripts', 'styles'], function() {
	  // watch for HTML changes
	  gulp.watch('./src/*.html', function() {
	    gulp.run('htmlpage');
	  });

	  // watch for JS changes
	  gulp.watch('./src/scripts/*.js', function() {
	    gulp.run('jshint', 'scripts');
	  });

	  // watch for CSS changes
	  gulp.watch('./src/styles/*.css', function() {
	    gulp.run('styles');
	  });
	});*/
